$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    $(".lista_permiso").on(tipoEvento, function(e) {
        e.preventDefault();
        var opcion = $(this).attr('id');
        var user = $(this).attr('data-user');
        var perfil = $(this).attr('data-perfil');
        var nombre = $(this).attr('data-nombre');
        activarPermiso(opcion, perfil, user, nombre);
    });
    $(".lista_inactivar").on(tipoEvento, function(e) {
        e.preventDefault();
        var opcion = $(this).attr('id');
        var user = $(this).attr('data-user');
        var perfil = $(this).attr('data-perfil');
        var nombre = $(this).attr('data-nombre');
        inactivarPermiso(opcion, perfil, user, nombre);
    });

    function activarPermiso(opcion, perfil, user, nombre) {
        try {
            $.ajax({
                url: '../vistas/ajax/permisos/activarPermiso.php',
                type: 'POST',
                data: {
                    'opcion': opcion,
                    'perfil': perfil,
                    'user': user
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == 'ok') {
                        $("#" + opcion).addClass('active disabled');
                        $("#" + opcion).html(nombre + '<i class="fa fa-times float-right"></i>');
                        ohSnap("Permiso concedido", {
                            color: "green",
                            duration: "1000"
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("Error al conceder permiso", {
                            color: "red"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function inactivarPermiso(opcion, perfil, user, nombre) {
        try {
            $.ajax({
                url: '../vistas/ajax/permisos/inactivarPermiso.php',
                type: 'POST',
                data: {
                    'opcion': opcion,
                    'perfil': perfil,
                    'user': user
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == 'ok') {
                        $("#" + opcion).addClass('disabled');
                        $("#" + opcion).removeClass('active');
                        $("#" + opcion).html(nombre + '<i class="fa fa-check float-right"></i>');
                        ohSnap("Permiso removido", {
                            color: "yellow",
                            duration: "1000"
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("Error al remover permiso", {
                            color: "red"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function recargarPagina() {
        window.location.replace("index");
    }
});