$(document).ready(function() {
	var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
	/*---------------------------------------------------*/
	$(".activar_area").on(tipoEvento, function() {
		var id = $(this).attr('id');
		var log = $(this).attr('data-log');
		activarArea(id, log);
	});

	function activarArea(id, log) {
		try {
			$.ajax({
				url: '../vistas/ajax/areas/activarAreas.php',
				type: 'POST',
				data: {
					'id': id,
				},
				cache: false,
				dataType: 'json',
				success: function(resultado) {
					if (resultado.mensaje == 'ok') {
						ohSnap("Area activada!", {
							color: "green",
							duration: "1000"
						});
						setTimeout(recargarPagina, 1050);
					} else {
						ohSnap("Error de activacion!", {
							color: "red",
							duration: "1000"
						});
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}
	/*---------------------------------------------------------*/
	/*---------------------------------------------------*/
	$(".inactivar_area").on(tipoEvento, function() {
		var id = $(this).attr('id');
		var log = $(this).attr('data-log');
		inactivarArea(id, log);
	});

	function inactivarArea(id, log) {
		try {
			$.ajax({
				url: '../vistas/ajax/areas/inactivarAreas.php',
				type: 'POST',
				data: {
					'id': id,
				},
				cache: false,
				dataType: 'json',
				success: function(resultado) {
					if (resultado.mensaje == 'ok') {
						ohSnap("Area eliminada!", {
							color: "red",
							duration: "1000"
						});
						setTimeout(recargarPagina, 1050);
					} else {
						ohSnap("Error de inactivacion!", {
							color: "red",
							duration: "1000"
						});
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}
	/*---------------------------------------------------------*/
	function recargarPagina() {
		window.location.replace("index");
	}
});