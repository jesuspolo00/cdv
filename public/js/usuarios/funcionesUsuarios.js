$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    $("#enviar_datos").on(tipoEvento, function(e) {
        e.preventDefault();
        var nombre = $("#nombre").val();
        var apellido = $("#apellido").val();
        var usuario = $("#user_name").val();
        var pass = $("#password").val();
        var conf_pass = $("#conf_password").val();
        var perfil = $("#perfil").val();
        if (usuario != "" && pass != "" && conf_pass != "" && nombre != "" && apellido != "" && perfil != "") {
            validarUsuario(usuario);
        } else if (conf_pass != pass) {
            ohSnap("Contraseñas incorrectas!", {
                color: "red",
                'duration': '1000'
            });
        } else {
            ohSnap("Favor llenar los campos obligatorios!", {
                color: "red",
                'duration': '1000'
            });
        }
    });
    /*---------------------------------------------------*/
    $(".activar_usuario").on(tipoEvento, function() {
        var id = $(this).attr('id');
        var log = $(this).attr('data-log');
        activarUsuario(id, log);
    });

    function activarUsuario(id, log) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuarios/activarUsuario.php',
                type: 'POST',
                data: {
                    'id': id,
                    'log': log,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == 'ok') {
                        ohSnap("Usuario activado!", {
                            color: "green",
                            duration: "1000"
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("Error de inactivacion!", {
                            color: "red",
                            duration: "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*---------------------------------------------------------*/
    /*---------------------------------------------------*/
    $(".inactivar_usuario").on(tipoEvento, function() {
        var id = $(this).attr('id');
        var log = $(this).attr('data-log');
        inactivarUsuario(id, log);
    });

    function inactivarUsuario(id, log) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuarios/inactivarUsuario.php',
                type: 'POST',
                data: {
                    'id': id,
                    'log': log,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == 'ok') {
                        ohSnap("Usuario eliminado!", {
                            color: "red",
                            duration: "1000"
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("Error de inactivacion!", {
                            color: "red",
                            duration: "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*---------------------------------------------------*/
    $(".restablecer_pass").on(tipoEvento, function() {
        var id = $(this).attr('id');
        restablecerPass(id);
    });

    function restablecerPass(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuarios/restablecerPass.php',
                type: 'POST',
                data: {
                    'id': id,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == 'ok') {
                        ohSnap("Contraseña restblecida!", {
                            color: "green",
                            duration: "1000"
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("Error de contraseña!", {
                            color: "red",
                            duration: "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*---------------------------------------------------------*/
    function validarUsuario(usuario) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuarios/validarUsuario.php',
                type: 'POST',
                data: {
                    'usuario': usuario,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == 'existe') {
                        ohSnap("El usuario ya existe", {
                            color: "red"
                        });
                        $("#usuario").focus();
                    } else {
                        $("#form_enviar").submit();
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function recargarPagina() {
        window.location.replace("index");
    }
});