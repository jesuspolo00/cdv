<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
  $er    = '2';
  $error = base64_encode($er);
  $salir = new Session;
  $salir->iniciar();
  $salir->outsession();
  header('Location:login?er=' . $error);
  exit();
}
include_once VISTA_PATH . 'cabeza.php';
require_once CONTROL_PATH . 'permisos' . DS . 'ControlPermisos.php';

$instancia_permiso = ControlPermisos::singleton_permisos();

$id_log           = $_SESSION['id'];
$id_perfil_sesion = $_SESSION['rol'];
?>
<!-- Sidebar -->
<ul class="navbar-nav bg-white sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?=BASE_URL?>inicio">
    <div class="sidebar-brand-icon">
      <img src="<?=PUBLIC_PATH?>img/icono.png" class="img-fluid" width="50">
    </div>
    <div class="sidebar-brand-text mx-3 text-secondary mt-3">
    </div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item active">
    <a class="nav-link" href="<?=BASE_URL?>inicio">
      <i class="fas fa-home text-secondary"></i>
      <span class="text-muted">Inicio</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider bg-gray">

    <?php
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 3);
    if ($permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>usuarios/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-users text-secondary"></i>
          <span class="text-muted">Usuarios</span>
        </a>
      </li>
    <?php }
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 4);
    if ($permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>areas/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-map-marker-alt text-secondary"></i>
          <span class="text-muted">CDV</span>
        </a>
      </li>
    <?php }
    /*$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 5);
    if ($permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>inventario/panel" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-barcode text-secondary"></i>
          <span class="text-muted">Reportar</span>
        </a>
      </li>
    <?php }*/
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 10);
    if ($permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-truck-loading text-secondary"></i>
          <span class="text-muted">Proveedor</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar" style="">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Acciones:</h6>
            <a class="collapse-item" href="<?=BASE_URL?>proveedor/index">Listado de proveedores</a>
            <a class="collapse-item" href="<?=BASE_URL?>proveedor/registro">Registrar proveedores</a>
          </div>
        </div>
      </li>
    <?php }

    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 11);
    if ($permiso && $_SESSION['rol'] > 2) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>solicitud/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-clipboard-list text-secondary"></i>
          <span class="text-muted">Solicitudes</span>
        </a>
      </li>
    <?php }
    if ($_SESSION['rol'] <= 2 && $permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTree" aria-expanded="true" aria-controls="collapseTree">
          <i class="fas fa-clipboard-list text-secondary"></i>
          <span class="text-muted">Solicitudes</span>
        </a>
        <div id="collapseTree" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar" style="">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Acciones:</h6>
            <a class="collapse-item" href="<?=BASE_URL?>solicitud/listado">Listado de solicitudes</a>
            <a class="collapse-item" href="<?=BASE_URL?>solicitud/index">Registrar solicitud</a>
          </div>
        </div>
      </li>
      <?php
    }
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 7);
    if ($permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>reportes/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-tools text-secondary"></i>
          <span class="text-muted">Listado de reportes</span>
        </a>
      </li>
    <?php }
    ?>
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block bg-gray">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

  </ul>
  <!-- End of Sidebar -->

  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

      <!-- Topbar -->
      <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow-none">


        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
          <i class="fa fa-bars text-secondary"></i>
        </button>

        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">


          <div class="topbar-divider d-none d-sm-block"></div>

          <!-- Nav Item - User Information -->
          <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?=$_SESSION['nombre_admin'] . ' ' . $_SESSION['apellido']?></span>
              <img class="img-profile rounded-circle" src="<?=PUBLIC_PATH?>img/user.svg">
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
              <a class="dropdown-item" href="<?=BASE_URL?>perfil/index">
                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                Perfil
              </a>
              <?php
              $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 1);
              if ($permiso) {
                ?>
                <a class="dropdown-item" href="<?=BASE_URL?>configuracion/index">
                  <i class="fas fa-cog fa-sm fa-fw mr-2 text-gray-400"></i>
                  Configuracion
                </a>
              <?php }?>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="<?=BASE_URL?>salir">
                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                Cerrar sesion
              </a>
            </div>
          </li>

        </ul>

      </nav>
      <!-- End of Topbar -->

      <!-- Begin Page Content -->
      <div class="container-fluid">

        <?php
        include_once VISTA_PATH . 'script_and_final.php';
      ?>