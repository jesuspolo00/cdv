<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
include_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 1);

if (!$permiso) {
	include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3">
					<h4 class="m-0 font-weight-bold text-success">Modulos</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<?php
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 3);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>usuarios/index">
								<div class="card border-left-success shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Usuarios</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-users fa-2x text-success"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
							<?php
						}
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 5);
						if ($permiso) {
							?>
							<!-- <a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>inventario/panel">
								<div class="card border-left-dark shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Reportar</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-barcode fa-2x text-dark"></i>
											</div>
										</div>
									</div>
								</div>
							</a> -->
							<?php
						}
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 4);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>areas/index">
								<div class="card border-left-orange shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">CDV</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-map-marker-alt fa-2x text-orange"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
							<?php
						}
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 7);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>reportes/index">
								<div class="card border-left-danger shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Listado de reportes</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-tools fa-2x text-danger"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
							<?php
						}

						/*$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 9);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>categorias/index">
								<div class="card border-left-info shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Categorias</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-cubes fa-2x text-info"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }*/
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 10);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>proveedor/index">
								<div class="card border-left-warning shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Proveedores</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-truck-loading fa-2x text-warning"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 11);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>solicitud/listado">
								<div class="card border-left-primary shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Solicitudes</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-clipboard-list fa-2x text-primary"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 2);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>permisos/index">
								<div class="card border-left-secondary shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Permisos</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-user-lock fa-2x text-secondary"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>