<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia = ControlAreas::singleton_areas();

$datos_areas = $instancia->mostrarAreasControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 4);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						CDV
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-dark"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 19px, 0px);">
							<div class="dropdown-header">Acciones:</div>
							<a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_area">Agregar CDV</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8 form-inline">
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" placeholder="Buscar">
									<div class="input-group-prepend">
										<span class="input-group-text rounded-right" id="basic-addon1">
											<i class="fa fa-search"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="table-responsive mt-2">
						<table class="table table-hover table-sm border" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">#</th>
									<th scope="col">Nombre</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_areas as $area) {
									$id_area = $area['id'];
									$nombre  = $area['nombre'];
									$activo  = $area['activo'];

									if ($activo == 1) {
										$icon   = '<i class="fa fa-times"></i>';
										$title  = 'Eliminar';
										$button = 'btn-danger';
										$clase  = 'inactivar_area';
									} else {
										$icon   = '<i class="fa fa-check"></i>';
										$title  = 'Activar';
										$button = 'btn-success';
										$clase  = 'activar_area';
									}

									$ver_inactivo_area = ($activo == 0 && $_SESSION['rol'] != 1) ? 'd-none' : '';

									?>
									<tr class="text-center text-dark <?=$ver_inactivo_area?>">
										<td><?=$id_area?></td>
										<td><?=$nombre?></td>
										<td>
											<button class="btn <?=$button?> btn-sm <?=$clase?>" data-tooltip="tooltip" data-placement="bottom" title="<?=$title?>" id="<?=$id_area?>" data-log="<?=$id_log?>">
												<?=$icon?>
											</button>
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'areas' . DS . 'agregarArea.php';

if (isset($_POST['nombre'])) {
	$instancia->agregarAreasControl();
}
?>
<script src="<?=PUBLIC_PATH?>/js/areas/funcionesAreas.js"></script>