<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'solicitud' . DS . 'ControlSolicitud.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia       = ControlSolicitud::singleton_solicitud();
$instancia_areas = ControlAreas::singleton_areas();

$datos_solicitud = $instancia->mostrarSolicitudesUsuarioControl($id_log);

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 11);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						Mis solicitudes realizadas
					</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8 form-inline">
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" placeholder="Buscar">
									<div class="input-group-prepend">
										<span class="input-group-text rounded-right" id="basic-addon1">
											<i class="fa fa-search"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">#</th>
									<th scope="col">CDV</th>
									<th scope="col">Usuario</th>
									<th scope="col">Justificacion</th>
									<th scope="col">Fecha solicitado</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_solicitud as $solicitud) {
									$id_solicitud  = $solicitud['id'];
									$id_area       = $solicitud['id_area'];
									$id_user       = $solicitud['id_user'];
									$nom_user      = $solicitud['nom_usuario'];
									$nom_area      = $solicitud['area_nom'];
									$estado        = $solicitud['estado'];
									$justificacion = $solicitud['justificacion'];

									$fechareg = ($estado == 3) ? $solicitud['fecha_aplazado'] : $solicitud['fecha_solicitud'];

									$ver_boton        = '';
									$ver_imprimir     = 'd-none';
									$span             = '<span class="badge badge-warning">Pendiente de revision</span>';
									$ver_verificacion = 'd-none';

									if ($estado == 1) {
										$span         = '<span class="badge badge-success">Aprobada</span>';
										$ver_boton    = 'd-none';
										$ver_imprimir = '';
									}

									if ($estado == 2) {
										$span         = '<span class="badge badge-danger">Rechazada</span>';
										$ver_boton    = 'd-none';
										$ver_imprimir = 'd-none';
									}

									if ($estado == 3) {
										$span         = '<span class="badge badge-secondary">Aplazada</span>';
										$ver_boton    = 'd-none';
										$ver_imprimir = 'd-none';
									}
									?>
									<tr class="text-center">
										<td><?=$id_solicitud?></td>
										<td><?=$nom_area?></td>
										<td><?=$nom_user?></td>
										<td><?=$justificacion?></td>
										<td><?=date('Y-m-d', strtotime($fechareg))?></td>
										<td>
											<?=$span?>
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>