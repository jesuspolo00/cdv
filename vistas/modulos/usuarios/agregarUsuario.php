<!--Agregar usuario-->
<div class="modal fade" id="agregar_usuario" tabindex="-1" role="modal" aria-hidden="true" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-lg p-2" role="document">
        <div class="modal-content">
            <form method="POST" id="form_enviar">
                <input type="hidden" value="<?=$id_log?>" name="id_log">
                <div class="modal-header p-3">
                    <h4 class="modal-title text-success font-weight-bold">Agregar Usuario</h4>
                </div>
                <div class="modal-body border-0">
                    <div class="row  p-3">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Documento <span class="text-danger">*</span></label>
                                <input type="text" class="form-control numeros" maxlength="50" minlength="1" name="documento" id="doc_user" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
                                <input type="text" class="form-control letras" maxlength="50" minlength="1" name="nombre" id="nombre" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Apellido <span class="text-danger">*</span></label>
                                <input type="text" class="form-control letras" maxlength="50" minlength="1" name="apellido" id="apellido">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Telefono</label>
                                <input type="text" class="form-control numeros" maxlength="50" minlength="1" name="telefono" id="telefono">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Correo</label>
                                <input type="email" class="form-control" maxlength="50" minlength="1" name="correo" id="correo">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Usuario <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" maxlength="50" minlength="1" name="usuario" id="user_name" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Contrase&ntilde;a <span class="text-danger">*</span></label>
                                <input type="password" class="form-control" maxlength="16" minlength="8" name="password" id="password" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Confirmar Contrase&ntilde;a <span class="text-danger">*</span></label>
                                <input type="password" class="form-control" maxlength="16" minlength="8" name="conf_password" id="conf_password" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Perfil <span class="text-danger">*</span></label>
                                <select class="form-control" name="perfil" id="perfil" required>
                                    <option selected value="">Seleccione una opcion...</option>
                                    <?php
                                    foreach ($datos_perfil as $perfiles) {
                                        $id_perfil  = $perfiles['id'];
                                        $nom_perfil = $perfiles['nombre'];

                                        $ver = ($id_perfil == 1 || $id_perfil == 5) ? 'd-none' : '';

                                        ?>
                                        <option value="<?=$id_perfil?>" class="<?=$ver?>"><?=$nom_perfil?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border-0">
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">
                        <i class="fa fa-times"></i>
                        &nbsp;
                        Cancelar
                    </button>
                    <button type="submit" class="btn btn-success btn-sm" id="enviar_datos">
                        <i class="fa fa-save"></i>
                        &nbsp;
                        Registrar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>