<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia        = ControlUsuario::singleton_usuario();
$instancia_perfil = ControlPerfil::singleton_perfil();

$datos_perfil   = $instancia_perfil->mostrarPerfilesControl();
$datos_usuarios = $instancia->mostrarUsuariosControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 3);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Usuarios
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-dark"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 19px, 0px);">
							<div class="dropdown-header">Acciones:</div>
							<a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_usuario">Agregar Usuario</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8 form-inline">
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" placeholder="Buscar">
									<div class="input-group-prepend">
										<span class="input-group-text rounded-right" id="basic-addon1">
											<i class="fa fa-search"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">#</th>
									<th scope="col">Documento</th>
									<th scope="col">Nombre completo</th>
									<th scope="col">Correo</th>
									<th scope="col">Telefono</th>
									<th scope="col">Usuario</th>
									<th scope="col">Perfil</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_usuarios as $usuario) {
									$id_user         = $usuario['id_user'];
									$documento       = $usuario['documento'];
									$nombre_completo = $usuario['nombre'] . ' ' . $usuario['apellido'];
									$correo          = $usuario['correo'];
									$telefono        = $usuario['telefono'];
									$user            = $usuario['user'];
									$perfil          = $usuario['nom_perfil'];
									$activo          = $usuario['estado'];

									if ($activo == 1) {
										$icon   = '<i class="fa fa-times"></i>';
										$title  = 'Eliminar';
										$button = 'btn-danger';
										$clase  = 'inactivar_usuario';
									} else {
										$icon   = '<i class="fa fa-check"></i>';
										$title  = 'Activar';
										$button = 'btn-success';
										$clase  = 'activar_usuario';
									}

									$ver_user          = ($usuario['perfil'] == 1) ? 'd-none' : '';
									$ver_inactivo_user = ($activo == 0 && $_SESSION['rol'] != 1) ? 'd-none' : '';
									$ver_proveedor     = ($usuario['perfil'] == 5 && $_SESSION['rol'] != 1) ? 'd-none' : '';

									?>
									<tr class="text-center text-dark <?=$ver_user . ' ' . $ver_inactivo_user . ' ' . $ver_proveedor?>">
										<td><?=$id_user?></td>
										<td><?=$documento?></td>
										<td><?=$nombre_completo?></td>
										<td><?=$correo?></td>
										<td><?=$telefono?></td>
										<td><?=$user?></td>
										<td><?=$perfil?></td>
										<td>
											<div class="btn-group btn-group-sm" role="group">
												<button class="btn btn-sm btn-primary" data-tooltip="tooltip" data-placement="bottom" title="Editar" data-toggle="modal" data-target="#usuario<?=$id_user?>">
													<i class="fa fa-user-edit"></i>
												</button>
												<button class="btn <?=$button?> btn-sm <?=$clase?>" data-tooltip="tooltip" data-placement="bottom" title="<?=$title?>" id="<?=$id_user?>" data-log="<?=$id_log?>">
													<?=$icon?>
												</button>
											</div>
										</td>
									</tr>


									<!-- Modal -->
									<div class="modal fade" id="usuario<?=$id_user?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title text-success font-weight-bold">Agregar usuario</h5>
												</div>
												<form method="POST">
													<input type="hidden" name="id_log" value="<?=$id_log?>">
													<input type="hidden" name="id_user" value="<?=$id_user?>">
													<div class="modal-body border-0">
														<div class="row p-2">
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Documento <span class="text-danger">*</span></label>
																<input type="text" name="documento_edit" class="form-control" value="<?=$documento?>" required>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
																<input type="text" name="nombre_edit" class="form-control" value="<?=$usuario['nombre']?>" required>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Apellido <span class="text-danger">*</span></label>
																<input type="text" name="apellido_edit" class="form-control" value="<?=$usuario['apellido']?>" required>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Telefono</label>
																<input type="text" name="telefono_edit" class="form-control" value="<?=$telefono?>">
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Correo</label>
																<input type="text" class="form-control" value="<?=$correo?>" name="correo_edit">
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Usuario <span class="text-danger">*</span></label>
																<input type="text" class="form-control" value="<?=$user?>" disabled>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Perfil <span class="text-danger">*</span></label>
																<select name="perfil_edit" class="form-control">
																	<option value="<?=$usuario['perfil']?>" class="d-none" selected><?=$perfil?></option>
																	<?php
																	foreach ($datos_perfil as $perfiles) {
																		$id_perfil  = $perfiles['id'];
																		$nom_perfil = $perfiles['nombre'];

																		$ver = ($id_perfil == 1) ? 'd-none' : '';

																		?>
																		<option value="<?=$id_perfil?>" class="<?=$ver?>"><?=$nom_perfil?></option>
																		<?php
																	}
																	?>
																</select>
															</div>
														</div>
													</div>
													<div class="modal-footer border-0">
														<div class="col-lg-12 form-group text-right">
															<button type="button" class="btn btn-primary btn-sm float-left restablecer_pass" data-toggle="popover" title="Restablecer contrase&ntilde;a" data-content="La nueva contrase&ntilde;a sera: cdv123456@" data-trigger="hover" data-placement="left" id="<?=$id_user?>">
																<i class="fas fa-sync-alt"></i>
																&nbsp;
																Restablecer contrase&ntilde;a
															</button>
															<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
																<i class="fa fa-times"></i>
																&nbsp;
																Cerrar
															</button>
															<button type="submit" class="btn btn-success btn-sm">
																<i class="fa fa-user-edit"></i>
																&nbsp;
																Editar
															</button>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>


									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include_once VISTA_PATH . 'script_and_final.php';
	include_once VISTA_PATH . 'modulos' . DS . 'usuarios' . DS . 'agregarUsuario.php';

	if (isset($_POST['documento'])) {
		$instancia->registrarUsuarioControl();
	}

	if (isset($_POST['documento_edit'])) {
		$instancia->actualizarUsuarioControl();
	}
	?>
	<script src="<?=PUBLIC_PATH?>js/usuarios/funcionesUsuarios.js"></script>