<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';
require_once CONTROL_PATH . 'categorias' . DS . 'ControlCategorias.php';
require_once CONTROL_PATH . 'proveedor' . DS . 'ControlProveedor.php';

$instancia           = ControlInventario::singleton_inventario();
$instancia_usuario   = ControlUsuario::singleton_usuario();
$instancia_area      = ControlAreas::singleton_areas();
$instancia_categoria = ControlCategorias::singleton_categoria();
$instancia_proveedor = ControlProveedor::singleton_proveedor();

$datos_usuarios  = $instancia_usuario->mostrarUsuariosControl();
$datos_areas     = $instancia_area->mostrarAreasControl();
$datos_categoria = $instancia_categoria->mostrarCategoriasControl();
$datos_proveedor = $instancia_proveedor->mostrarProveedoresControl();

if (isset($_POST['buscar'])) {
	$id_area         = $_POST['area'];
	$id_usuario      = '';
	$texto           = $_POST['texto'];
	$datos_articulos = $instancia->mostrarArticulosPanelControl($id_area, $id_usuario, $texto);
} else {
	$datos_articulos = $instancia->mostrarTodosArticulosControl();
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 5);

if (!$permiso) {
	include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						Panel de control - Reportar
					</h4>
					<button class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#agregar_articulo">
						<i class="fa fa-plus"></i>
						&nbsp;
						Agregar articulo
					</button>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-5 form-group">
								<select class="form-control" name="area">
									<option value="" selected>Seleccione un area...</option>
									<?php
									foreach ($datos_areas as $area) {
										$id_area     = $area['id'];
										$nombre      = $area['nombre'];
										$activo_area = $area['activo'];

										$ver_area = ($activo_area == 1) ? '' : 'd-none';
										?>
										<option value="<?=$id_area?>" class="<?=$ver_area?>"><?=$nombre?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-5 form-group">
								<input type="text" class="form-control filtro" placeholder="Buscar por articulo..." name="texto">
							</div>
							<div class="col-lg-2 form-group mt-1 text-center">
								<button class="btn btn-primary btn-sm" type="submit" name="buscar" data-tooltip="tooltip" data-placement="bottom" title="Buscar">
									<i class="fa fa-search"></i>
									&nbsp;
									Buscar
								</button>
								<a href="<?=BASE_URL?>inventario/cantidades" class="btn btn-secondary btn-sm" type="submit" data-tooltip="tooltip" data-placement="bottom" title="Ver cantidades">
									<i class="fa fa-eye"></i>
								</a>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover table-sm border" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">Area</th>
									<th scope="col">Descripcion</th>
									<th scope="col">Marca</th>
									<th scope="col">Codigo</th>
									<th scope="col">Categoria</th>
									<th scope="col">Estado</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								if ($datos_articulos == "") {
									?>
									<tr class="text-center text-uppercase text-dark">
										<td colspan="8">No hay resultados que mostrar.</td>
									</tr>
									<?php
								} else {
									foreach ($datos_articulos as $articulo) {
										$id_articulo  = $articulo['id'];
										$descripcion  = $articulo['descripcion'];
										$area         = $articulo['nom_area'];
										$estado       = $articulo['nom_estado'];
										$usuario      = $articulo['nom_user'];
										$marca        = $articulo['marca'];
										$codigo       = $articulo['codigo'];
										$id_usuario   = $articulo['id_user'];
										$id_categoria = $articulo['id_categoria'];

										$id_estado = $articulo['estado'];

										$hoja_vida = ($articulo['hoja_vida'] == 0) ? $descripcion : '												<a href="' . BASE_URL . 'inventario/hojaVida?inventario=' . base64_encode($id_articulo) . '">' . $descripcion . '</a>';

										$ver_button = ($id_estado == 1 || $id_estado == 4 || $id_estado == 7) ? '' : 'd-none';

										$ver_reasignar = ($id_estado == 6) ? '' : 'd-none';

										$ver_descontinuar = ($id_estado == 5) ? '' : 'd-none';

										$ver_mantenimiento = ($articulo['hoja_vida'] != 0) ? '' : 'd-none';

										$estado_span = ($id_estado == 1) ? '<span class="badge badge-primary">Nuevo</span>' : '';
										$estado_span = ($id_estado == 2) ? '<span class="badge badge-danger">Dañado</span>' : $estado_span;
										$estado_span = ($id_estado == 3) ? '<span class="badge badge-warning">Mantenimiento</span>' : $estado_span;
										$estado_span = ($id_estado == 4) ? '<span class="badge badge-success">Arreglado</span>' : $estado_span;
										$estado_span = ($id_estado == 5) ? '<span class="badge badge-danger">Descontinuado</span>' : $estado_span;
										$estado_span = ($id_estado == 6) ? '<span class="badge badge-info">Liberado</span>' : $estado_span;
										$estado_span = ($id_estado == 7) ? '<span class="badge badge-info">Reasignado</span>' : $estado_span;

										?>
										<tr class="text-center text-uppercase text-dark">
											<td><?=$area?></td>
											<td><?=$hoja_vida?></td>
											<td><?=$marca?></td>
											<td><?=$codigo?></td>
											<td><?=$articulo['nom_categoria']?></td>
											<td><?=$estado_span?></td>
											<td>
												<a href="<?=BASE_URL?>inventario/hojaVida?inventario=<?=base64_encode($id_articulo)?>" class="btn btn-info btn-sm" data-tooltip="tooltip" title="Hoja de vida" data-palcement="bottom" data-trigger="hover">
													<i class="fa fa-eye"></i>
												</a>
												<div class="btn-group btn-group-sm <?=$ver_button?>" role="group">
													<button class="btn btn-success btn-sm" data-tooltip="tooltip" title="Reportar de daño" data-placement="bottom" data-toggle="modal" data-target="#reporte<?=$id_articulo?>">
														<i class="fas fa-clipboard-check"></i>
													</button>
													<button class="btn btn-warning btn-sm <?=$ver_mantenimiento?>" data-tooltip="tooltip" title="Reportar mantenimiento" data-placement="bottom" data-toggle="modal" data-target="#mantenimiento<?=$id_articulo?>">
														<i class="fas fa-wrench"></i>
													</button>
													<button class="btn btn-info btn-sm" data-placement="bottom" data-tooltip="tooltip" title="Cambiar area" data-toggle="modal" data-target="#liberar<?=$id_articulo?>">
														<i class="fab fa-telegram-plane"></i>
													</button>
													<button class="btn btn-danger btn-sm" data-placement="bottom" data-tooltip="tooltip" title="Dar de baja" data-toggle="modal" data-target="#descontinuar<?=$id_articulo?>">
														<i class="fa fa-times"></i>
													</button>
												</div>
											</td>
											<td class="<?=$ver_reasignar?>">
												<button class="btn btn-info btn-sm" data-toggle="modal" data-target="#reasignar<?=$id_articulo?>">
													<i class="fas fa-sync-alt"></i>
													&nbsp;
													Reasignar
												</button>
											</td>
										</tr>



										<!-- Reporte de daño -->
										<div class="modal fade" id="reporte<?=$id_articulo?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Reportar daño</h5>
													</div>
													<form method="POST">
														<input type="hidden" name="id_log" value="<?=$id_log?>">
														<input type="hidden" name="id_usuario" value="<?=$id_usuario?>">
														<input type="hidden" name="id_articulo" value="<?=$id_articulo?>">
														<input type="hidden" name="inicio" value="0">
														<input type="hidden" name="estado" value="2">
														<div class="modal-body border-0">
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Descripcion</label>
																<input type="text" class="form-control" disabled value="<?=$descripcion?>">
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Area</label>
																<input type="text" class="form-control" disabled value="<?=$area?>">
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Codigo</label>
																<input type="text" class="form-control" disabled value="<?=$codigo?>">
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Fecha da&ntilde;o <span class="text-danger">*</span></label>
																<input type="date" class="form-control" name="fecha_reporte" required>
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Observacion</label>
																<textarea class="form-control" name="observacion" rows="5" cols="5"></textarea>
															</div>
														</div>
														<div class="modal-footer border-0">
															<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
																<i class="fa fa-times"></i>
																&nbsp;
																Cancelar
															</button>
															<button type="submit" class="btn btn-success btn-sm">
																<i class="fas fa-clipboard-check"></i>
																&nbsp;
																Reportar
															</button>
														</div>
													</form>
												</div>
											</div>
										</div>



										<!-- Reporte de daño -->
										<div class="modal fade" id="mantenimiento<?=$id_articulo?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Reportar mantenimiento</h5>
													</div>
													<form method="POST">
														<input type="hidden" name="id_log" value="<?=$id_log?>">
														<input type="hidden" name="id_usuario" value="<?=$id_usuario?>">
														<input type="hidden" name="id_articulo" value="<?=$id_articulo?>">
														<input type="hidden" name="estado" value="3">
														<input type="hidden" name="inicio" value="0">
														<div class="modal-body border-0">
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Descripcion</label>
																<input type="text" class="form-control" disabled value="<?=$descripcion?>">
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Area</label>
																<input type="text" class="form-control" disabled value="<?=$area?>">
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Codigo</label>
																<input type="text" class="form-control" disabled value="<?=$codigo?>">
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Fecha mantenimiento <span class="text-danger">*</span></label>
																<input type="date" class="form-control" name="fecha_reporte" required>
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Observacion</label>
																<textarea class="form-control" name="observacion" rows="5" cols="5"></textarea>
															</div>
														</div>
														<div class="modal-footer border-0">
															<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
																<i class="fa fa-times"></i>
																&nbsp;
																Cancelar
															</button>
															<button type="submit" class="btn btn-success btn-sm">
																<i class="fas fa-clipboard-check"></i>
																&nbsp;
																Reportar
															</button>
														</div>
													</form>
												</div>
											</div>
										</div>




										<!-- Descontinuar -->
										<div class="modal fade" id="reasignar<?=$id_articulo?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Reasignar articulo</h5>
													</div>
													<form method="POST">
														<input type="hidden" name="id_log" value="<?=$id_log?>">
														<input type="hidden" name="id_articulo_reg" value="<?=$id_articulo?>">
														<input type="hidden" name="usuario_reasignar" value="0">
														<div class="modal-body border-0">
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Descripcion</label>
																<input type="text" class="form-control" disabled value="<?=$descripcion?>">
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Codigo</label>
																<input type="text" class="form-control" disabled value="<?=$codigo?>">
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Fecha reasignacion <span class="text-danger">*</span></label>
																<input type="date" class="form-control" name="fecha_reporte" required>
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Area <span class="text-danger">*</span></label>
																<select name="area_reasignar" class="form-control" required>
																	<option value="" selected>Seleccione un area...</option>
																	<?php
																	foreach ($datos_areas as $areas) {
																		$id_area     = $areas['id'];
																		$nombre      = $areas['nombre'];
																		$activo_area = $areas['activo'];

																		$ver_area = ($activo_area == 1) ? '' : 'd-none';
																		?>
																		<option value="<?=$id_area?>" class="<?=$ver_area?>"><?=$nombre?></option>
																		<?php
																	}
																	?>
																</select>
															</div>
														</div>
														<div class="modal-footer border-0">
															<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
																<i class="fa fa-times"></i>
																&nbsp;
																Cancelar
															</button>
															<button type="submit" class="btn btn-success btn-sm">
																<i class="fas fa-sync-alt"></i>
																&nbsp;
																Reasignar
															</button>
														</div>
													</form>
												</div>
											</div>
										</div>


										<!-- Liberar -->
										<div class="modal fade" id="liberar<?=$id_articulo?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Liberar articulo</h5>
													</div>
													<form method="POST">
														<input type="hidden" name="id_log" value="<?=$id_log?>">
														<input type="hidden" name="id_usuario" value="<?=$id_usuario?>">
														<input type="hidden" name="id_articulo" value="<?=$id_articulo?>">
														<input type="hidden" name="estado" value="6">
														<input type="hidden" name="inicio" value="0">
														<input type="hidden" name="fecha_reporte" value="">
														<div class="modal-body border-0">
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Descripcion</label>
																<input type="text" class="form-control" disabled value="<?=$descripcion?>">
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Area</label>
																<input type="text" class="form-control" disabled value="<?=$area?>">
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Codigo</label>
																<input type="text" class="form-control" disabled value="<?=$codigo?>">
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Observacion</label>
																<textarea class="form-control" name="observacion" rows="5" cols="5"></textarea>
															</div>
														</div>
														<div class="modal-footer border-0">
															<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
																<i class="fa fa-times"></i>
																&nbsp;
																Cancelar
															</button>
															<button type="submit" class="btn btn-success btn-sm">
																<i class="fab fa-telegram-plane"></i>
																&nbsp;
																Liberar
															</button>
														</div>
													</form>
												</div>
											</div>
										</div>



										<!-- Liberar -->
										<div class="modal fade" id="descontinuar<?=$id_articulo?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Descontinuar articulo</h5>
													</div>
													<form method="POST">
														<input type="hidden" name="id_log" value="<?=$id_log?>">
														<input type="hidden" name="id_usuario" value="<?=$id_usuario?>">
														<input type="hidden" name="id_articulo" value="<?=$id_articulo?>">
														<input type="hidden" name="estado" value="5">
														<input type="hidden" name="inicio" value="0">
														<input type="hidden" name="fecha_reporte" value="">
														<div class="modal-body border-0">
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Descripcion</label>
																<input type="text" class="form-control" disabled value="<?=$descripcion?>">
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Area</label>
																<input type="text" class="form-control" disabled value="<?=$area?>">
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Codigo</label>
																<input type="text" class="form-control" disabled value="<?=$codigo?>">
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Observacion</label>
																<textarea class="form-control" name="observacion" rows="5" cols="5"></textarea>
															</div>
														</div>
														<div class="modal-footer border-0">
															<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
																<i class="fa fa-times"></i>
																&nbsp;
																Cancelar
															</button>
															<button type="submit" class="btn btn-success btn-sm">
																<i class="fa fa-times"></i>
																&nbsp;
																Descontinuar
															</button>
														</div>
													</form>
												</div>
											</div>
										</div>

										<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'inventario' . DS . 'agregarInventario.php';
include_once VISTA_PATH . 'modulos' . DS . 'inventario' . DS . 'agregarInventario.php';

if (isset($_POST['descripcion'])) {
	$instancia->agregarArticuloControl();
}

if (isset($_POST['id_articulo'])) {
	$instancia->reportarArticuloControl();
}

if (isset($_POST['usuario_reasignar'])) {
	$instancia->reasignarArticuloControl();
}
?>
