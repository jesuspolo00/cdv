<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';
require_once CONTROL_PATH . 'proveedor' . DS . 'ControlProveedor.php';

$instancia           = ControlInventario::singleton_inventario();
$instancia_area      = ControlAreas::singleton_areas();
$instancia_proveedor = ControlProveedor::singleton_proveedor();

$datos_area      = $instancia_area->mostrarAreasControl();
$datos_estado    = $instancia->mostrarEstadosControl();
$datos_proveedor = $instancia_proveedor->mostrarProveedoresControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 6);

if (!$permiso) {
	include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
	exit();
}

if (!isset($_GET['inventario'])) {
	include_once VISTA_PATH . DS . 'modulos' . DS . '404.php';
	exit();
}

$id_inventario = base64_decode($_GET['inventario']);

$datos_hoja       = $instancia->hojaVidaArticuloControl($id_inventario);
$datos_reporte    = $instancia->mostrarReportesControl($id_inventario);
$datos_hardware   = $instancia->mostrarHardwareInventarioControl($id_inventario);
$datos_componente = $instancia->mostrarComponentesInventarioControl();
$datos_software   = $instancia->mostrarSoftwareInventarioControl($id_inventario);

$fecha_update = ($datos_hoja['fecha_update'] == '') ? $datos_hoja['fecha_hoja'] : $datos_hoja['fecha_update'];

/*onclick="window.history.go(-1); return false;"*/

?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>inventario/panel"  class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Hoja de vida (<?=$datos_hoja['descripcion']?>)
					</h4>
					<h6 class="text-right mt-2 font-weight-bold  text-success">Fecha de ultima actualizacion: <?=$fecha_update?></h6>
				</div>
				<div class="card-body">
					<form method="POST">
						<input type="hidden" value="<?=$id_log?>" name="id_log" id="id_log">
						<input type="hidden" value="<?=$datos_hoja['id_hoja']?>" name="id_hoja" id="id_hoja">
						<input type="hidden" value="<?=$id_inventario?>" name="id_inventario">
						<div class="row">
							<div class="col-lg-4">
								<div class="form-group">
									<label class="font-weight-bold">Nombre del equipo <span class="text-danger">*</span></label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" maxlength="50" required name="descripcion" value="<?=$datos_hoja['descripcion']?>">
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label class="font-weight-bold">Marca</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" maxlength="50" name="marca" value="<?=$datos_hoja['marca']?>">
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label class="font-weight-bold">Modelo</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" maxlength="50" name="modelo" value="<?=$datos_hoja['modelo']?>">
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label class="font-weight-bold">Codigo inventario</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" disabled value="<?=$datos_hoja['codigo']?>">
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label class="font-weight-bold">Dependencia <span class="text-danger">*</span></label>
									<div class="input-group input-group-sm mb-3">
										<select class="form-control" name="area">
											<option value="<?=$datos_hoja['id_area']?>" class="d-none" selected><?=$datos_hoja['nom_area']?></option>
											<?php
											foreach ($datos_area as $area) {
												$id_area  = $area['id'];
												$nom_area = $area['nombre'];
												?>
												<option value="<?=$id_area?>"><?=$nom_area?></option>
												<?php
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label class="font-weight-bold">Estado</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" disabled value="<?=$datos_hoja['nom_estado']?>">
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label class="font-weight-bold">Proveedor</label>
									<div class="input-group input-group-sm mb-3">
										<select name="proveedor" class="form-control">
											<option value="<?=$datos_hoja['proveedor']?>" class=d-none><?=$datos_hoja['nom_proveedor'] . ' (' . $datos_hoja['num_proveedor'] . ')'?></option>
											<?php
											foreach ($datos_proveedor as $proveedor) {
												$id_proveedor   = $proveedor['id_proveedor'];
												$nombre         = $proveedor['razon_social'];
												$identificacion = $proveedor['num_identificacion'];
												$telefono       = $proveedor['telefono'];
												$nom_contacto   = $proveedor['contacto'];
												$tel_contacto   = $proveedor['telefono_contacto'];
												$detalle        = $proveedor['detalle_producto'];
												$fecha_ingreso  = $proveedor['fecha_ingreso'];
												?>
												<option value="<?=$id_proveedor?>"><?=$nombre . ' (' . $identificacion . ')'?></option>
											<?php }?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label class="font-weight-bold">Fecha de adquisicion</label>
									<div class="input-group input-group-sm mb-3">
										<input type="date" class="form-control" name="fecha_compra" value="<?=$datos_hoja['fecha_compra']?>">
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label class="font-weight-bold">Frecuencia de mantenimiento (meses)</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control numeros" name="frecuencia_mantenimiento" value="<?=$datos_hoja['frecuencia_mantenimiento']?>">
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label class="font-weight-bold">Fecha vencimiento garantia</label>
									<div class="input-group input-group-sm mb-3">
										<input type="date" class="form-control" name="fecha_garantia" value="<?=$datos_hoja['fecha_garantia']?>">
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label class="font-weight-bold">Contacto garantia</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" name="contacto_garantia" value="<?=$datos_hoja['contacto_garantia']?>">
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group mt-2">
									<input type="hidden" value="<?=$datos_hoja['codigo']?>" name="codigo" id="codigo">
									<a href="<?=PUBLIC_PATH?>upload/<?=$datos_hoja['codigo']?>.png" class="btn btn-primary btn-sm mt-4" download="" id="desc_codigo">
										<i class="fas fa-barcode"></i>
										&nbsp;
										Descargar codigo
									</a>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="table-responsive">
									<table class="table table-hover border" width="100%" cellspacing="0">
										<thead>
											<tr class="text-center font-weight-bold">
												<th colspan="4">
													Componentes de hardware
												</th>
												<th>
													<button type="button" class="btn btn-success btn-sm float-right" data-tooltip="tooltip" data-placement="left" title="Agregar hardware" data-toggle="modal" data-target="#agregar_hardware">
														<i class="fa fa-plus"></i>
													</button>
												</th>
											</tr>
											<tr class="text-center font-weight-bold">
												<th>Descripcion</th>
												<th>Modelo</th>
												<th>Marca</th>
												<th>Codigo</th>
											</tr>
										</thead>
										<tbody class="buscar text-uppercase">
											<?php
											foreach ($datos_hardware as $hard) {
												$id_hard = $hard['id'];
												$nombre  = $hard['descripcion'];
												$marca   = $hard['marca'];
												$modelo  = $hard['modelo'];
												$codigo  = $hard['codigo'];
												?>
												<tr class="text-center text-dark remov<?=$id_hard?>">
													<td><?=$nombre?></td>
													<td><?=$modelo?></td>
													<td><?=$marca?></td>
													<td><?=$codigo?></td>
													<td>
														<button type="button" class="btn btn-danger btn-sm liberar_hardw" id="<?=$id_hard?>" data-inventario="<?=$id_inventario?>" data-log="<?=$id_log?>" data-tooltip="tooltip" data-placement="bottom" title="Remover">
															<i class="fa fa-times"></i>
														</button>
													</td>
												</tr>
												<?php
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="table-responsive">
									<table class="table table-hover border" width="100%" cellspacing="0">
										<thead>
											<tr class="text-center font-weight-bold">
												<th colspan="4">
													Componentes de software
												</th>
												<th>
													<button type="button" class="btn btn-success btn-sm float-right" data-tooltip="tooltip" data-placement="left" title="Agregar software" data-toggle="modal" data-target="#agregar_soft">
														<i class="fa fa-plus"></i>
													</button>
												</th>
											</tr>
											<tr class="text-center font-weight-bold">
												<th>Descripcion</th>
												<th>Version</th>
												<th>Fabricante</th>
												<th>Licencia</th>
											</tr>
										</thead>
										<tbody class="buscar text-uppercase">
											<?php
											foreach ($datos_software as $software) {
												$id_soft     = $software['id'];
												$descripcion = $software['descripcion'];
												$fabricante  = $software['fabricante'];
												$version     = $software['version'];
												$licencia    = $software['licencia'];

												?>
												<tr class="text-center text-dark">
													<td><?=$descripcion?></td>
													<td><?=$version?></td>
													<td><?=$fabricante?></td>
													<td><?=$licencia?></td>
													<td>
														<button class="btn btn-danger btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Remover">
															<i class="fa fa-times"></i>
														</button>
													</td>
												</tr>
												<?php
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="table-responsive">
									<table class="table table-hover border" width="100%" cellspacing="0">
										<thead>
											<tr class="text-center font-weight-bold">
												<th colspan="2">
													Ubicacion y asignacion del equipo
												</th>
											</tr>
											<tr class="text-center font-weight-bold">
												<th>Area responsable</th>
												<th>Fecha de asignacion</th>
											</tr>
										</thead>
										<tbody class="buscar text-uppercase">
											<tr class="text-center text-dark">
												<td><?=$datos_hoja['nom_area']?></td>
												<td><?=$datos_hoja['fechareg']?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="table-responsive">
									<table class="table table-hover border" width="100%" cellspacing="0">
										<thead>
											<tr class="text-center font-weight-bold">
												<th colspan="6">
													Control de mantenimientos
												</th>
											</tr>
											<tr class="text-center font-weight-bold">
												<th>Observacion</th>
												<th>Reporte</th>
												<th>Fecha</th>
											</tr>
										</thead>
										<tbody class="buscar text-uppercase">
											<?php
											foreach ($datos_reporte as $reporte) {
												$id_reporte    = $reporte['id_reporte'];
												$area          = $reporte['nom_area'];
												$descripcion   = $reporte['descripcion'];
												$marca         = $reporte['marca'];
												$modelo        = $reporte['modelo'];
												$codigo        = $reporte['codigo'];
												$estado        = $reporte['nom_estado'];
												$observacion   = $reporte['observacion'];
												$usuario       = $reporte['usuario'];
												$id_usuario    = $reporte['id_user'];
												$id_articulo   = $reporte['id_inventario'];
												$fecha_reporte = $reporte['fecha_reporte'];

												if ($reporte['id_respuesta'] == '') {
													$respuesta = '';
												} else {

													$fecha_original = $instancia->mostrarFechaReportadoControl($reporte['id_respuesta']);
													$datetime1      = new DateTime($fecha_original['fechareg']);
													$datetime2      = new DateTime($fecha_reporte);
													$interval       = $datetime1->diff($datetime2);
													$respuesta      = $interval->format('%d Dias %h Horas %i Minutos %s Segundos');
												}

												?>
												<tr class="text-center text-dark">
													<td><?=$observacion?></td>
													<td><?=$estado?></td>
													<td><?=date('Y-m-d', strtotime($fecha_reporte))?></td>
												</tr>
											<?php }?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-lg-12 form-group mt-4">
								<a class="btn btn-secondary btn-sm float-left" href="<?=BASE_URL?>imprimir/hoja_vida?inventario=<?=base64_encode($id_inventario)?>" target="_blank">
									<i class="fa fa-print"></i>
									&nbsp;
									Imprimir
								</a>
								<button class="btn btn-success btn-sm float-right" type="submit">
									<i class="fa fa-save"></i>
									&nbsp;
									Guardar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'inventario' . DS . 'agregarComponente.php';

if (isset($_POST['id_log'])) {
	$instancia->actualizarHojaVidaControl();
}

if (isset($_POST['id_user'])) {
	$instancia->agregarHardwareControl();
}

if (isset($_POST['descripcion_soft'])) {
	$instancia->agregarSoftwareControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/inventario/funcionesInventario.js"></script>