<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';
require_once CONTROL_PATH . 'categorias' . DS . 'ControlCategorias.php';

$instancia           = ControlInventario::singleton_inventario();
$instancia_usuario   = ControlUsuario::singleton_usuario();
$instancia_area      = ControlAreas::singleton_areas();
$instancia_categoria = ControlCategorias::singleton_categoria();

$datos_usuarios  = $instancia_usuario->mostrarUsuariosControl();
$datos_areas     = $instancia_area->mostrarAreasControl();
$datos_categoria = $instancia_categoria->mostrarCategoriasControl();

if (isset($_POST['buscar'])) {
	$id_area         = $_POST['area'];
	$id_usuario      = $_POST['usuario'];
	$datos_articulos = $instancia->mostrarArticulosControl($id_area, $id_usuario);
} else {
	$datos_articulos = "";
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 5);

if (!$permiso) {
	include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Inventario
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 19px, 0px);">
							<div class="dropdown-header">Acciones:</div>
							<a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_articulo">Agregar Articulo</a>
							<a class="dropdown-item" href="<?=BASE_URL?>inventario/panel">Panel de control</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-5 form-group">
								<select class="form-control" name="area">
									<option value="" selected>Seleccione un area...</option>
									<?php
									foreach ($datos_areas as $area) {
										$id_area     = $area['id'];
										$nombre      = $area['nombre'];
										$activo_area = $area['activo'];

										$ver_area = ($activo_area == 1) ? '' : 'd-none';
										?>
										<option value="<?=$id_area?>" class="<?=$ver_area?>"><?=$nombre?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-5 form-group">
								<select class="form-control" name="usuario">
									<option value="" selected>Seleccione un usuario...</option>
									<?php
									foreach ($datos_usuarios as $usuario) {
										$id_user = $usuario['id_user'];
										$nombre  = $usuario['nombre'] . ' ' . $usuario['apellido'];
										$activo  = $usuario['estado'];

										$ver = ($activo == 1) ? '' : 'd-none';
										?>
										<option value="<?=$id_user?>" class="<?=$ver?>"><?=$nombre?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-2 form-group mt-1 text-center">
								<button class="btn btn-primary btn-sm" type="submit" name="buscar">
									<i class="fa fa-search"></i>
									&nbsp;
									Buscar
								</button>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">Area</th>
									<th scope="col">Usuario</th>
									<th scope="col">Descripcion</th>
									<th scope="col">Cant.</th>
									<th scope="col">Marca</th>
									<th scope="col">Estado</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								if ($datos_articulos == "") {
									?>
									<tr class="text-center text-uppercase">
										<td colspan="8">No hay resultados que mostrar.</td>
									</tr>
									<?php
								} else {
									foreach ($datos_articulos as $articulo) {
										$id_articulo = $articulo['id'];
										$nombre      = $articulo['descripcion'];
										$area        = $articulo['nom_area'];
										$estado      = $articulo['nom_estado'];
										$usuario     = $articulo['nom_user'];
										$cantidad    = $articulo['cantidad'];
										$marca       = $articulo['marca'];

										/*$hoja_vida = ($articulo['hoja_vida'] == 0) ? $nombre : '                                                <a href="' . BASE_URL . 'inventario/hojaVida?inventario=' . base64_encode($id_articulo) . '">' . $nombre . '</a>';*/

										?>
										<tr class="text-center text-uppercase text-dark">
											<td><?=$area?></td>
											<td><?=$usuario?></td>
											<td><?=$nombre?></td>
											<td><?=$cantidad?></td>
											<td><?=$marca?></td>
											<td><?=$estado?></td>
										</tr>
										<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'inventario' . DS . 'agregarInventario.php';

if (isset($_POST['descripcion'])) {
	$instancia->agregarArticuloControl();
}
?>
