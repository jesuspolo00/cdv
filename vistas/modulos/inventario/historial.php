<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';

$instancia = ControlInventario::singleton_inventario();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 8);

if (!$permiso) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}

if (isset($_GET['inventario'])) {

    $id_inventario = base64_decode($_GET['inventario']);

    $datos_articulo = $instancia->hojaVidaArticuloControl($id_inventario);
    $datos_reporte  = $instancia->mostrarReportesControl($id_inventario);
    ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow-sm mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h4 class="m-0 font-weight-bold text-primary">
                            <a href="#" onclick="window.history.go(-1); return false;" class="text-decoration-none">
                                <i class="fa fa-arrow-left text-primary"></i>
                            </a>
                            &nbsp;
                            Detalle articulo (<?=$datos_articulo['descripcion']?>)
                        </h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-weight-bold">Nombre del equipo</label>
                                    <div class="input-group input-group-sm mb-3">
                                        <input type="text" class="form-control" maxlength="50" disabled value="<?=$datos_articulo['descripcion']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-weight-bold">Marca</label>
                                    <div class="input-group input-group-sm mb-3">
                                        <input type="text" class="form-control" maxlength="50" disabled value="<?=$datos_articulo['marca']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-weight-bold">Modelo</label>
                                    <div class="input-group input-group-sm mb-3">
                                        <input type="text" class="form-control" maxlength="50" disabled value="<?=$datos_articulo['modelo']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-weight-bold">Codigo</label>
                                    <div class="input-group input-group-sm mb-3">
                                        <input type="text" class="form-control" maxlength="50" disabled value="<?=$datos_articulo['codigo']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-weight-bold">Area</label>
                                    <div class="input-group input-group-sm mb-3">
                                        <input type="text" class="form-control" maxlength="50" disabled value="<?=$datos_articulo['nom_area']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-weight-bold">Usuario responsable</label>
                                    <div class="input-group input-group-sm mb-3">
                                        <input type="text" class="form-control" maxlength="50" disabled value="<?=$datos_articulo['usuario']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-weight-bold">Fecha asignado</label>
                                    <div class="input-group input-group-sm mb-3">
                                        <input type="text" class="form-control" maxlength="50" disabled value="<?=$datos_articulo['fechareg']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group mt-2">
                                    <input type="hidden" value="<?=$datos_articulo['codigo']?>" name="codigo" id="codigo">
                                    <a href="<?=PUBLIC_PATH?>upload/<?=$datos_articulo['codigo']?>.png" class="btn btn-primary btn-sm mt-4" download="" id="desc_codigo">
                                        <i class="fas fa-barcode"></i>
                                        &nbsp;
                                        Descargar codigo
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive mt-4">
                            <table class="table table-hover mt-2 table-borderless table-sm" width="100%" cellspacing="0">
                                <thead>
                                    <tr class="text-center font-weight-bold border">
                                        <td scope="col" colspan="9">Historial de reportes / mantenimientos</td>
                                    </tr>
                                    <tr class="text-center font-weight-bold">
                                        <th scope="col">Reporte</th>
                                        <th scope="col">Observacion</th>
                                        <th scope="col">Fecha</th>
                                    </tr>
                                </thead>
                                <tbody class="buscar text-uppercase">
                                    <?php
                                    foreach ($datos_reporte as $reporte) {
                                        $id_reporte    = $reporte['id_reporte'];
                                        $area          = $reporte['nom_area'];
                                        $descripcion   = $reporte['descripcion'];
                                        $marca         = $reporte['marca'];
                                        $modelo        = $reporte['modelo'];
                                        $codigo        = $reporte['codigo'];
                                        $estado        = $reporte['nom_estado'];
                                        $observacion   = $reporte['observacion'];
                                        $usuario       = $reporte['usuario'];
                                        $id_usuario    = $reporte['id_user'];
                                        $id_articulo   = $reporte['id_inventario'];
                                        $fecha_reporte = $reporte['fecha_reporte'];

                                        if ($reporte['id_respuesta'] == '') {
                                            $respuesta = '';
                                        } else {

                                            $fecha_original = $instancia->mostrarFechaReportadoControl($reporte['id_respuesta']);
                                            $datetime1      = new DateTime($fecha_original['fechareg']);
                                            $datetime2      = new DateTime($fecha_reporte);
                                            $interval       = $datetime1->diff($datetime2);
                                            $respuesta      = $interval->format('%d Dias %h Horas %i Minutos %s Segundos');
                                        }

                                        ?>
                                        <tr class="text-center text-dark">
                                            <td><?=$estado?></td>
                                            <td><?=$observacion?></td>
                                            <td><?=date('Y-m-d', strtotime($fecha_reporte))?></td>
                                        </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    include_once VISTA_PATH . 'script_and_final.php';
}
?>
<script src="<?=PUBLIC_PATH?>js/inventario/funcionesInventario.js"></script>