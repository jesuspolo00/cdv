<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';
require_once CONTROL_PATH . 'categorias' . DS . 'ControlCategorias.php';

$instancia           = ControlInventario::singleton_inventario();
$instancia_usuario   = ControlUsuario::singleton_usuario();
$instancia_area      = ControlAreas::singleton_areas();
$instancia_categoria = ControlCategorias::singleton_categoria();

$datos_usuarios  = $instancia_usuario->mostrarUsuariosControl();
$datos_areas     = $instancia_area->mostrarAreasControl();
$datos_categoria = $instancia_categoria->mostrarCategoriasControl();

if (isset($_POST['buscar'])) {
	$id_area         = $_POST['area'];
	$id_usuario      = '';
	$texto           = $_POST['texto'];
	$datos_articulos = $instancia->mostrarArticulosCantidadPanelControl($id_area, $id_usuario, $texto);
	$cantidades      = $instancia->cantidadesAreaControl($id_area);

	$cantidad_activo        = 0;
	$cantidad_descontinuado = 0;
	$cantidad_mantenimiento = 0;
	$cantidad_liberado      = 0;

	foreach ($cantidades as $numero_cant) {
		$cantidad_activo += $numero_cant['cantidad_activo'];
		$cantidad_descontinuado += $numero_cant['cantidad_descontinuado'];
		$cantidad_mantenimiento += $numero_cant['cantidad_mantenimiento'];
		$cantidad_liberado += $numero_cant['cantidad_liberado'];
	}

} else {
	$datos_articulos = $instancia->mostrarCantidadesArticulosControl();
	$cantidades      = $instancia->cantidadesGeneralControl();

	$cantidad_activo        = 0;
	$cantidad_descontinuado = 0;
	$cantidad_mantenimiento = 0;
	$cantidad_liberado      = 0;

	foreach ($cantidades as $numero_cant) {
		$cantidad_activo += $numero_cant['cantidad_activo'];
		$cantidad_descontinuado += $numero_cant['cantidad_descontinuado'];
		$cantidad_mantenimiento += $numero_cant['cantidad_mantenimiento'];
		$cantidad_liberado += $numero_cant['cantidad_liberado'];
	}
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 5);

if (!$permiso) {
	include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>inventario/panel" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Cantidades de inventario (Area - General)
					</h4>
					<small class="m-0 float-right text-danger">
						Nota: esta vista solo es para consultar cantidades por areas y categorias.
					</small>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-5 form-group">
								<select class="form-control" name="area">
									<option value="" selected>Seleccione un area...</option>
									<?php
									foreach ($datos_areas as $area) {
										$id_area     = $area['id'];
										$nombre      = $area['nombre'];
										$activo_area = $area['activo'];

										$ver_area = ($activo_area == 1) ? '' : 'd-none';
										?>
										<option value="<?=$id_area?>" class="<?=$ver_area?>"><?=$nombre?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-5 form-group">
								<input type="text" class="form-control filtro" placeholder="Buscar por articulo..." name="texto">
							</div>
							<div class="col-lg-2 form-group mt-1 text-center">
								<button class="btn btn-primary btn-sm" type="submit" name="buscar" data-tooltip="tooltip" data-placement="bottom" title="Buscar">
									<i class="fa fa-search"></i>
									&nbsp;
									Buscar
								</button>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<div class="form-inline col-lg-12">
							<h6 class="text-primary font-weight-bold mt-1 mb-4 ml-5">Cantidad articulos (Activos): <?=$cantidad_activo?></h6>
							<h6 class="text-danger font-weight-bold mt-1 mb-4 ml-5">Cantidad articulos (Descontinuados): <?=$cantidad_descontinuado?></h6>
							<h6 class="text-warning font-weight-bold mt-1 mb-4 ml-5">Cantidad articulos (Mantenimiento): <?=$cantidad_mantenimiento?></h6>
							<h6 class="text-info font-weight-bold mt-1 mb-4 ml-5">Cantidad articulos (Liberados): <?=$cantidad_liberado?></h6>
						</div>
						<table class="table table-hover table-sm border" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">Area</th>
									<th scope="col">Descripcion</th>
									<th scope="col">Marca</th>
									<th scope="col">Modelo</th>
									<th scope="col">Categoria</th>
									<th scope="col">Cantidades</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								if ($datos_articulos == "") {
									?>
									<tr class="text-center text-uppercase text-dark">
										<td colspan="8">No hay resultados que mostrar.</td>
									</tr>
									<?php
								} else {
									foreach ($datos_articulos as $articulo) {
										$descripcion = $articulo['descripcion'];
										$area        = $articulo['nom_area'];
										$marca       = $articulo['marca'];
										$modelo      = $articulo['modelo'];
										$categoria   = $articulo['nom_categoria'];
										$cantidad    = $articulo['cantidad'];
										?>
										<tr class="text-center text-uppercase text-dark">
											<td><?=$area?></td>
											<td><?=$descripcion?></td>
											<td><?=$marca?></td>
											<td><?=$modelo?></td>
											<td><?=$categoria?></td>
											<td><?=$cantidad?></td>
										</tr>
										<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';