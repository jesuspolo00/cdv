<!--Agregar usuario-->
<div class="modal fade" id="agregar_articulo" tabindex="-1" role="modal" aria-hidden="true" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-lg p-2" role="document">
        <div class="modal-content">
            <form method="POST">
                <input type="hidden" value="<?=$id_log?>" name="id_log">
                <div class="modal-header p-3">
                    <h4 class="modal-title text-success font-weight-bold">Agregar Articulo</h4>
                </div>
                <div class="modal-body border-0">
                    <div class="row  p-3">
                        <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Descripcion <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" maxlength="50" name="descripcion" required>
                        </div>
                        <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Marca</label>
                            <input type="text" class="form-control" maxlength="50" name="marca">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Modelo</label>
                            <input type="text" class="form-control" maxlength="50" name="modelo">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Proveedor</label>
                            <select class="form-control" name="proveedor">
                                <option value="" selected>Seleccione una opcion...</option>
                                <?php
                                foreach ($datos_proveedor as $proveedor) {
                                    $id_proveedor   = $proveedor['id_proveedor'];
                                    $nombre         = $proveedor['razon_social'];
                                    $identificacion = $proveedor['num_identificacion'];
                                    $telefono       = $proveedor['telefono'];
                                    $nom_contacto   = $proveedor['contacto'];
                                    $tel_contacto   = $proveedor['telefono_contacto'];
                                    $detalle        = $proveedor['detalle_producto'];
                                    $fecha_ingreso  = $proveedor['fecha_ingreso'];
                                    ?>
                                    <option value="<?=$id_proveedor?>"><?=$nombre . ' (' . $identificacion . ')'?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Fecha de adquisicion</label>
                            <input type="date" class="form-control" maxlength="50" name="fecha_compra">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Frecuencia de mantenimiento (Meses)</label>
                            <input type="text" class="form-control numeros" name="frecuencia_man" value="6">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Fecha vencimiento garantia</label>
                            <input type="date" class="form-control" name="fecha_vence">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Contacto garantia</label>
                            <input type="text" class="form-control" maxlength="50" name="contacto_garantia">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Area / Oficina <span class="text-danger">*</span></label>
                            <select name="area" class="form-control" required>
                                <option value="" selected>Seleccione una opcion...</option>
                                <?php
                                foreach ($datos_areas as $area) {
                                    $id_area     = $area['id'];
                                    $nombre      = $area['nombre'];
                                    $activo_area = $area['activo'];

                                    $ver_area = ($activo_area == 1) ? '' : 'd-none';
                                    ?>
                                    <option value="<?=$id_area?>" class="<?=$ver_area?>"><?=$nombre?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Categoria <span class="text-danger">*</span></label>
                            <select name="categoria" class="form-control" required>
                                <option value="" selected>Seleccione una opcion...</option>
                                <?php
                                foreach ($datos_categoria as $categoria) {
                                    $id_categoria = $categoria['id'];
                                    $nombre_categ = $categoria['nombre'];
                                    $activo       = $categoria['activo'];

                                    $ver_cat = ($activo == 1) ? '' : 'd-none';

                                    ?>
                                    <option value="<?=$id_categoria?>" class="<?=$ver_cat?>"><?=$nombre_categ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Cantidad <span class="text-danger">*</span></label>
                            <input type="text" class="form-control numeros" maxlength="50" name="cantidad" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border-0">
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">
                        <i class="fa fa-times"></i>
                        &nbsp;
                        Cancelar
                    </button>
                    <button type="submit" class="btn btn-success btn-sm">
                        <i class="fa fa-save"></i>
                        &nbsp;
                        Registrar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>