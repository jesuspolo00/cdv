<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
require_once CONTROL_PATH . 'permisos' . DS . 'ControlPermisos.php';

$instancia        = ControlPermisos::singleton_permisos();
$instancia_perfil = ControlPerfil::singleton_perfil();

$datos_perfil   = $instancia_perfil->mostrarPerfilesControl();
$datos_opciones = $instancia->mostrarOpcionesPermisosControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 2);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Permisos
					</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8 form-inline">
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" placeholder="Buscar">
									<div class="input-group-prepend">
										<span class="input-group-text rounded-right" id="basic-addon1">
											<i class="fa fa-search"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="table-responsive mt-2">
						<table class="table table-hover table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">#</th>
									<th scope="col">Perfil</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php

								foreach ($datos_perfil as $key) {
									$id_perfil = $key['id'];
									$nombre    = $key['nombre'];
									?>
									<tr class="text-center text-dark">
										<td><?=$id_perfil?></td>
										<td><?=$nombre?></td>
										<td>
											<button class="btn btn-success btn-sm" data-tooltip="tooltip" title="Agregar Permiso" data-placement="bottom"
											data-toggle="modal" data-target="#permisos<?=$id_perfil?>">
											<i class="fa fa-plus"></i>
										</button>
									</td>
								</tr>


								<!-- Modal -->
								<div class="modal fade" id="permisos<?=$id_perfil?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Modulos</h5>
												<button type="button" class="btn btn-sm" data-dismiss="modal">
													<i class="fa fa-times"></i>
												</button>
											</div>
											<form method="POST">
												<input type="hidden" name="id_log" value="<?=$id_log?>">
												<div class="modal-body border-0">
													<div class="row p-2">
														<?php
														foreach ($datos_opciones as $opcion) {
															$id_opcion = $opcion['id'];
															$opcion    = $opcion['nombre'];

															$opcions_activos_perfil = $instancia->opcionsIdActivosPerfilControl($id_perfil, $id_opcion);

															if ($opcions_activos_perfil['id'] != "") {
																$activo = 'active';
																$icon   = '<i class="fa fa-times float-right"></i>';
																$class  = 'lista_inactivar';
															} else {
																$activo = '';
																$icon   = '<i class="fa fa-check float-right"></i>';
																$class  = 'lista_permiso';
															}

															?>
															<div class="col-lg-12 mb-2">
																<div class="list-group">
																	<a href="#" class="list-group-item list-group-item-action <?=$class?>  <?=$activo;?>" data-perfil="<?=$id_perfil;?>" data-user="<?=$id_log;?>" id="<?=$id_opcion;?>" data-nombre="<?=$opcion?>">
																		<?=$opcion;?>
																		<?=$icon;?>
																	</a>
																</div>
															</div>
															<?php
														}
														?>
													</div>
												</form>
											</div>
										</div>
									</div>


									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>
<script src="<?=PUBLIC_PATH?>/js/permisos/funcionesPermisos.js"></script>