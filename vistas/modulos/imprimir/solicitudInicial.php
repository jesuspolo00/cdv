<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once LIB_PATH . 'bardcode' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'solicitud' . DS . 'ControlSolicitud.php';
require_once CONTROL_PATH . 'proveedor' . DS . 'ControlProveedor.php';

$instancia           = ControlSolicitud::singleton_solicitud();
$instancia_proveedor = ControlProveedor::singleton_proveedor();

if (isset($_GET['solicitud'])) {

    $id_solicitud = base64_decode($_GET['solicitud']);

    $datos_solicitud    = $instancia->mostrarDatosSolicitudInicialIdControl($id_solicitud);
    $productos          = $instancia->mostrarProdcutosSolicitudInicialControl($id_solicitud);
    $datos_veriifcacion = $instancia->mostrarDatosVerificacionInicialControl($id_solicitud);
    $datos_proveedor    = $instancia_proveedor->mostrarDatosProveedorIdControl($datos_solicitud['id_proveedor']);

    $cumple_cant    = ($datos_veriifcacion['cantidad'] == 'Si') ? 'X' : '';
    $no_cumple_cant = ($datos_veriifcacion['cantidad'] == 'No') ? 'X' : '';

    $cumple_calidad    = ($datos_veriifcacion['calidad'] == 'Si') ? 'X' : '';
    $no_cumple_calidad = ($datos_veriifcacion['calidad'] == 'No') ? 'X' : '';

    $cumple_precio    = ($datos_veriifcacion['precios'] == 'Si') ? 'X' : '';
    $no_cumple_precio = ($datos_veriifcacion['precios'] == 'No') ? 'X' : '';

    $cumple_plazo    = ($datos_veriifcacion['plazos'] == 'Si') ? 'X' : '';
    $no_cumple_plazo = ($datos_veriifcacion['plazos'] == 'No') ? 'X' : '';

    class MYPDF extends TCPDF
    {

        public function setData($logo)
        {
            $this->logo = $logo;
        }

        public function Header()
        {
/*        $this->setJPEGQuality(90);
$this->Image(PUBLIC_PATH . 'img/' . $this->logo, 0, 0, 200, 35);
$this->Ln(30);
$this->Cell(90);
$this->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
$this->Cell(12, 50, 'ENTREGA DE INVENTARIO', 0, 0, 'C');*/
}

public function Footer()
{
    $this->SetY(-15);
    $this->SetFillColor(127);
    $this->SetTextColor(127);
    $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
    $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
}
}

// create a PDF object
$pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document (meta) information
$pdf->SetCreator(PDF_CREATOR);
$pdf->setData('encabezado.png');
$pdf->SetAuthor('Jesus Polo');
$pdf->SetTitle('Inventario');
$pdf->SetSubject('Inventario');
$pdf->SetKeywords('Inventario');
$pdf->AddPage();

$pdf->Ln(-6);
$pdf->Cell(10);
$pdf->Cell(320, 5, 'No. ' . $datos_solicitud['incremental_anio'], '', 0, 'C');
$pdf->Ln(10);
$pdf->Cell(10);
$pdf->Image(PUBLIC_PATH . 'img/logo.png', '', '', 25, 12, '', '', 'T', false, 90, '', false, false, 1, false, false, false);
$pdf->Ln(-5);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(142.5, 5, 'CDV', 'B', 0, 'C');
$pdf->Ln(6);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
$pdf->Cell(142.5, 5, 'SOLICITUD DE COMPRA', 'B', 0, 'C');
$pdf->Ln(6);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(47.5, 5, 'Codigo: RG-GEC-05', 'B', 0, 'C');
$pdf->Cell(47.5, 5, 'Version: 4', 'B', 0, 'C');
$pdf->Cell(47.5, 5, 'Fecha Version: 2021-04-15', 'B', 0, 'C');

$pdf->Ln(15);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
$pdf->Cell(180, 5, 'Señores: ' . $datos_proveedor['nombre'], 1, 0, 'L');

$ln = 5;
$pdf->Ln($ln);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
$pdf->Cell(180, 5, 'Nit: ' . $datos_proveedor['num_identificacion'], 1, 0, 'L');

$ln = 5;
$pdf->Ln($ln);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
$pdf->Cell(90, 5, 'Direccion: ' . $datos_proveedor['direccion'], 1, 0, 'L');
$pdf->Cell(90, 5, 'Tel / Fax: ' . $datos_proveedor['telefono'], 1, 0, 'L');

$ln = 5;
$pdf->Ln($ln);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
$pdf->Cell(180, 5, 'Fecha: ' . $datos_solicitud['fecha_solicitud'], 1, 0, 'L');

$tabla = '
<table border="1" cellpadding="3" style="font-size:8.5px; width:98%;">
<tr style="text-align:center; font-weight:bold;">
<th style="width: 10%;">Cantidad</th>
<th style="width: 90%;">Descripcion</th>
</tr>
';

$subtotal = 0;
$iva      = $datos_solicitud['iva'];

foreach ($productos as $producto) {
    $id_producto = $producto['id'];
    $nombre      = $producto['producto'];
    $cantidad    = $producto['cantidad'];
    $precio      = $producto['precio'];

    $total_unidad = ($precio * $cantidad);
    $subtotal += $total_unidad;

    $tabla .= '
    <tr style="text-align: center;">
    <td>' . $cantidad . '</td>
    <td>' . $nombre . '</td>
    </tr>
    ';

}

if ($datos_solicitud['firmas'] == 1) {

    $tabla .= '
    <tr>
    <td  style="width: 50%;"><span style="font-weight:bold;">Nombre de quien elabora:</span></td>
    <td  style="width: 50%;"><span style="font-weight:bold;">Nombre de quien aprueba:</span></td>
    </tr>
    <tr>
    <td  style="width: 50%;"><span style="font-weight:bold;">Nombre de quien solicita:</span> ' . $datos_solicitud['nom_usuario'] . '</td>
    <td  style="width: 50%;"><span style="font-weight:bold;">Firma del director financiero:</span></td>
    </tr>
    ';

}

$tabla .= '</table>';

$pdf->Ln(15);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($tabla, true, false, true, false, '');
/*-----------------------------------------------------*/

/*    if ($datos_veriifcacion['id'] != '') {

$tabla_verificacion = '
<table border="1" cellpadding="3" style="font-size:8.5px; width:98%;">
<tr style="text-align:center; font-weight:bold;">
<th colspan="4">VERIFICACION DE PRODUCTOS Y/O SERVICIOS ADQUIRIDOS</th>
</tr>
<tr  style="text-align:center; font-weight:bold;">
<th style="width: 30%;">ITEM</th>
<th style="width: 10%;">CUMPLE</th>
<th style="width: 10%;">NO CUMPLE</th>
<th style="width: 50%;">OBSERVACIONES</th>
</tr>
<tr>
<td style="text-align: center;">CANTIDAD</td>
<td style="text-align: center;">' . $cumple_cant . '</td>
<td style="text-align: center;">' . $no_cumple_cant . '</td>
<td>' . $datos_veriifcacion['observacion_cant'] . '</td>
</tr>
<tr>
<td>CALIDAD (Bienes y Servicios recibidos con especificaciones requeridas)</td>
<td style="text-align: center;">' . $cumple_calidad . '</td>
<td style="text-align: center;">' . $no_cumple_calidad . '</td>
<td>' . $datos_veriifcacion['observacion_calidad'] . '</td>
</tr>
<tr>
<td>PRECIOS PACTADOOS</td>
<td style="text-align: center;">' . $cumple_precio . '</td>
<td style="text-align: center;">' . $no_cumple_precio . '</td>
<td>' . $datos_veriifcacion['observacion_precios'] . '</td>
</tr>
<tr>
<td>PLAZOS DE ENTREGA</td>
<td style="text-align: center;">' . $cumple_plazo . '</td>
<td style="text-align: center;">' . $no_cumple_plazo . '</td>
<td>' . $datos_veriifcacion['observacion_plazo'] . '</td>
</tr>
<tr>
<td><span style="font-weight: bold;">NOMBRE DEL RESPONSABLE: </span></td>
<td colspan="3">' . $datos_veriifcacion['nom_usuario'] . '</td>
</tr>
<tr>
<td ><span style="font-weight: bold;">FECHA DE VERFICACION: </span></td>
<td colspan="3">' . $datos_veriifcacion['fecha_verificacion'] . '</td>
</tr>
</table>
';

$pdf->Ln(1);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($tabla_verificacion, true, false, true, false, '');

}*/

$ln = 5;
$pdf->Ln($ln);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 7);
$pdf->MultiCell(180, 5, '-Codetec se reserva el derecho de rechazar los productos  o servicios de esta orden de compra por cualquier causa que la haga insatisfactoria, dentro de los 30 dias  siguientes al recibo de la misma.', 0, 'L', 0, 0, '', '', true);

$pdf->Ln($ln + 3);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 7);
$pdf->MultiCell(180, 5, '-Recepcion de productos o servicios solamente en horarios de  oficina.', 0, 'L', 0, 0, '', '', true);

$pdf->Ln($ln);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 7);
$pdf->MultiCell(180, 5, '-El despacho de esta orden de compra implica aceptacion de estas condiciónes.', 0, 'L', 0, 0, '', '', true);

$style = array(
    'position'     => 'C',
    'align'        => 'C',
    'stretch'      => false,
    'fitwidth'     => true,
    'cellfitalign' => '',
    'border'       => false,
    'hpadding'     => 'auto',
    'vpadding'     => 'auto',
    'fgcolor'      => array(0, 0, 0),
        'bgcolor'      => false, //array(255,255,255),
        'text'         => true,
        'font'         => 'helvetica',
        'fontsize'     => 8,
        'stretchtext'  => 4,
    );

$nombre_archivo = PUBLIC_PATH_ARCH . 'upload' . DS . 'solicitud_inicial_' . md5($id_solicitud);

    //$pdf->Output($nombre_archivo . '.pdf', 'F');
$pdf->Output($nombre_archivo . '.pdf', 'I');
}
