<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'proveedor' . DS . 'ControlProveedor.php';

$instancia = ControlProveedor::singleton_proveedor();

$datos_proveedor = $instancia->mostrarProveedoresControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 10);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['proveedor'])) {

	$id_proveedor = base64_decode($_GET['proveedor']);

	$datos_proveedor        = $instancia->mostrarDatosProveedorIdControl($id_proveedor);
	$contactos_proveedor    = $instancia->mostrarContactosProveedorControl($id_proveedor);
	$banco_proveedor        = $instancia->mostrarBancoProveedorControl($id_proveedor);
	$documentos_proveedor   = $instancia->mostrarDocumentosProveedorControl($id_proveedor);
	$calificacion_proveedor = $instancia->mostrarCalificacionProveedorControl($id_proveedor);
	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-success">
							<a href="<?=BASE_URL?>proveedor/index"  class="text-decoration-none">
								<i class="fa fa-arrow-left text-success"></i>
							</a>
							&nbsp;
							Hoja de registro (<?=$datos_proveedor['nombre']?>)
						</h4>
						<h6 class="text-right mt-2 font-weight-bold  text-success">Fecha de ultima actualizacion: <?=$datos_proveedor['fechareg']?></h6>
					</div>
					<div class="card-body">
						<form method="POST">
							<input type="hidden" value="<?=$id_log?>" name="id_log" id="id_log">
							<input type="hidden" value="<?=$id_proveedor?>" name="id_proveedor" id="id_proveedor">
							<div class="row">

								<div class="form-group col-lg-12 text-center">
									<h5 class="font-weight-bold">INFORMACION DEL PROVEEDOR EXTERNO</h5>
									<hr>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Nombre o razon social <span class="text-danger">*</span></label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" maxlength="50" required name="nombre" value="<?=$datos_proveedor['nombre']?>">
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Identificacion <span class="text-danger">*</span></label>
									<div class="input-group input-group-sm mb-3">
										<select class="form-control" name="identificacion" required>
											<option value="<?=$datos_proveedor['identificacion']?>" class="d-none" selected><?=$datos_proveedor['identificacion']?></option>
											<option value="Nit">Nit</option>
											<option value="Cedula de ciudadania">Cedula de ciudadania</option>
											<option value="Cedula de extranjeria">Cedula de extranjeria</option>
										</select>
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Numero de identificacion <span class="text-danger">*</span></label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" maxlength="50" required name="num_identificacion" value="<?=$datos_proveedor['num_identificacion']?>">
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Direccion <span class="text-danger">*</span></label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" maxlength="50" required name="direccion" value="<?=$datos_proveedor['direccion']?>">
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Ciudad <span class="text-danger">*</span></label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" maxlength="50" required name="ciudad" value="<?=$datos_proveedor['ciudad']?>">
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Departamento <span class="text-danger">*</span></label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" maxlength="50" required name="departamento" value="<?=$datos_proveedor['departamento']?>">
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Pais <span class="text-danger">*</span></label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" maxlength="50" required name="pais" value="<?=$datos_proveedor['pais']?>">
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Telefono <span class="text-danger">*</span></label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" maxlength="50" required name="telefono" value="<?=$datos_proveedor['telefono']?>">
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Correo <span class="text-danger">*</span></label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" maxlength="50" required name="correo" value="<?=$datos_proveedor['correo']?>">
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Fecha ingreso <span class="text-danger">*</span></label>
									<div class="input-group input-group-sm mb-3">
										<input type="date" class="form-control" maxlength="50" required name="fecha_ingreso" value="<?=$datos_proveedor['fecha_ingreso']?>">
									</div>
								</div>


								<div class="form-group col-lg-12 text-center mt-5">
									<h5 class="font-weight-bold">INFORMACION DEL PRODUCTO</h5>
									<hr>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Tipo</label>
									<div class="input-group input-group-sm mb-3">
										<select class="form-control" name="tipo">
											<option value="<?=$datos_proveedor['tipo']?>" class="d-none"><?=$datos_proveedor['tipo']?></option>
											<option value="Bien">Bien</option>
											<option value="Servicio">Servicio</option>
											<option value="Bien y Servicio">Bien y Servicio</option>
										</select>
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Tiempo de entrega (Dias)</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control numeros" maxlength="50" name="tiempo_entrega" value="<?=$datos_proveedor['tiempo_entrega']?>">
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Garantia (Dias/Meses)</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control numeros" maxlength="50" name="garantia" value="<?=$datos_proveedor['garantia']?>">
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Plazo de pago (Dias/Meses)</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control numeros" maxlength="50" name="plazo_pago" value="<?=$datos_proveedor['plazo_pago']?>">
									</div>
								</div>
								<div class="form-group col-lg-12">
									<label class="font-weight-bold">Detalle del producto</label>
									<div class="input-group input-group-sm mb-3">
										<textarea class="form-control" rows="5" name="detalle_producto"><?=$datos_proveedor['detalle_producto']?></textarea>
									</div>
								</div>



								<div class="form-group col-lg-12 text-center mt-5">
									<h5 class="font-weight-bold">INFORMACION DEL REPRESENTANTE LEGAL</h5>
									<hr>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Nombre completo</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" name="nom_representante" value="<?=$datos_proveedor['nom_representante']?>">
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Identificacion</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control numeros" name="identificacion_representante" value="<?=$datos_proveedor['identificacion_representante']?>">
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Correo electronico</label>
									<div class="input-group input-group-sm mb-3">
										<input type="email" class="form-control" name="correo_representante" value="<?=$datos_proveedor['correo_representante']?>">
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Telefono</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" name="telefono_representante" value="<?=$datos_proveedor['telefono_representante']?>">
									</div>
								</div>


								<div class="form-group col-lg-12 text-center mt-5">
									<h5 class="font-weight-bold">INFORMACIÓN TRIBUTARIA</h5>
									<hr>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Regimen</label>
									<div class="input-group input-group-sm mb-3">
										<select class="form-control" name="regimen_proveedor">
											<option value="<?=$datos_proveedor['regimen_proveedor']?>" selected class="d-none"><?=$datos_proveedor['regimen_proveedor']?></option>
											<option value="Comun">Comun</option>
											<option value="Simplificado">Simplificado</option>
										</select>
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Gran contribuyente</label>
									<div class="input-group input-group-sm mb-3">
										<select class="form-control" name="contribuyente_proveedor">
											<option value="<?=$datos_proveedor['contribuyente_proveedor']?>" selected class="d-none"><?=$datos_proveedor['contribuyente_proveedor']?></option>
											<option value="Si">Si</option>
											<option value="No">No</option>
										</select>
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Autoretenedor</label>
									<div class="input-group input-group-sm mb-3">
										<select class="form-control" name="autoretenedor_proveedor">
											<option value="<?=$datos_proveedor['autoretenedor_proveedor']?>" selected class="d-none"><?=$datos_proveedor['autoretenedor_proveedor']?></option>
											<option value="Si">Si</option>
											<option value="No">No</option>
										</select>
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Responsable industria y comercio</label>
									<div class="input-group input-group-sm mb-3">
										<select class="form-control" name="comercio_proveedor">
											<option value="<?=$datos_proveedor['comercio_proveedor']?>" selected class="d-none"><?=$datos_proveedor['comercio_proveedor']?></option>
											<option value="Si">Si</option>
											<option value="No">No</option>
										</select>
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Actividad economica</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" name="actividad_proveedor" value="<?=$datos_proveedor['actividad_proveedor']?>">
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Tarifa</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" name="tarifa_proveedor" value="<?=$datos_proveedor['tarifa_proveedor']?>">
									</div>
								</div>




								<div class="form-group col-lg-12 text-center mt-5">
									<h5 class="font-weight-bold">REFERENCIA COMERCIAL</h5>
									<hr>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Nombre o razon social</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" name="comercial_nombre" value="<?=$datos_proveedor['comercial_nombre']?>">
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Identificacion</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" name="identificacion_comercial" value="<?=$datos_proveedor['identificacion_comercial']?>">
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Correo electronico</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" name="correo_comercial" value="<?=$datos_proveedor['correo_comercial']?>">
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Telefono</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control numeros" name="telefono_comercial" value="<?=$datos_proveedor['telefono_comercial']?>">
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Direccion</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" name="direccion_comercial" value="<?=$datos_proveedor['direccion_comercial']?>">
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Ciudad</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" name="ciudad_comercial" value="<?=$datos_proveedor['ciudad_comercial']?>">
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Departamento</label>
									<div class="input-group input-group-sm mb-3">
										<input type="text" class="form-control" name="departamento_comercial" value="<?=$datos_proveedor['departamento_comercial']?>">
									</div>
								</div>
							</div>


							<div class="col-lg-12 form-group mt-4">
								<div class="table-responsive">
									<table class="table table-hover border" width="100%" cellspacing="0">
										<thead>
											<tr class="text-center font-weight-bold">
												<th colspan="4">
													INFORMACIÓN DE CONTACTOS
												</th>
												<th>
													<button type="button" class="btn btn-success btn-sm float-right" data-tooltip="tooltip" data-placement="left" title="Agregar contacto" data-toggle="modal" data-target="#agregar_contacto">
														<i class="fa fa-plus"></i>
													</button>
												</th>
											</tr>
											<tr class="text-center font-weight-bold">
												<th>Nombre</th>
												<th>Telefono</th>
												<th>Correo</th>
												<th>Cargo</th>
											</tr>
										</thead>
										<tbody class="buscar text-uppercase">
											<?php
											foreach ($contactos_proveedor as $contacto) {
												$id_contacto = $contacto['id'];
												$nombre      = $contacto['nombre_contacto'];
												$telefono    = $contacto['telefono_contacto'];
												$correo      = $contacto['correo_contacto'];
												$cargo       = $contacto['cargo_contacto'];
												$activo      = $contacto['activo'];

												$ver = ($activo == 1) ? '' : 'd-none';
												?>
												<tr class="text-center text-dark <?=$ver?> contacto<?=$id_contacto?>">
													<td><?=$nombre?></td>
													<td><?=$telefono?></td>
													<td><?=$correo?></td>
													<td><?=$cargo?></td>
													<td>
														<button type="button" class="btn btn-danger btn-sm eliminar_contacto" data-tooltip="tooltip" data-placement="bottom" title="Eliminar" id="<?=$id_contacto?>">
															<i class="fa fa-times"></i>
														</button>
													</td>
												</tr>
												<?php
											}
											?>
										</tbody>
									</table>
								</div>
							</div>



							<div class="col-lg-12 form-group mt-4">
								<div class="table-responsive">
									<table class="table table-hover border" width="100%" cellspacing="0">
										<thead>
											<tr class="text-center font-weight-bold">
												<th colspan="3">
													INFORMACIÓN BANCARIA PARA EFECTUAR PAGOS
												</th>
												<th>
													<button type="button" class="btn btn-success btn-sm float-right" data-tooltip="tooltip" data-placement="left" title="Agregar banco" data-toggle="modal" data-target="#agregar_banco">
														<i class="fa fa-plus"></i>
													</button>
												</th>
											</tr>
											<tr class="text-center font-weight-bold">
												<th>Nombre</th>
												<th>Numero de cuenta</th>
												<th>Tipo de cuenta</th>
											</tr>
										</thead>
										<tbody class="buscar text-uppercase">
											<?php
											foreach ($banco_proveedor as $banco) {
												$id_banco = $banco['id'];
												$nombre   = $banco['nom_banco'];
												$numero   = $banco['num_banco'];
												$tipo     = $banco['tipo_cuenta'];
												$activo   = $banco['activo'];

												$ver = ($activo == 1) ? '' : 'd-none';
												?>
												<tr class="text-center text-dark <?=$ver?> banco<?=$id_banco?>">
													<td><?=$nombre?></td>
													<td><?=$numero?></td>
													<td><?=$tipo?></td>
													<td>
														<button type="button" class="btn btn-danger btn-sm eliminar_banco" data-tooltip="tooltip" data-placement="bottom" title="Eliminar" id="<?=$id_banco?>">
															<i class="fa fa-times"></i>
														</button>
													</td>
												</tr>
												<?php
											}
											?>
										</tbody>
									</table>
								</div>
							</div>


							<div class="col-lg-12 form-group mt-4">
								<div class="table-responsive">
									<table class="table table-hover border" width="100%" cellspacing="0">
										<thead>
											<tr class="text-center font-weight-bold">
												<th colspan="2">
													DOCUMENTACION LEGAL
												</th>
											</tr>
											<tr class="text-center font-weight-bold">
												<th>Tipo de documento</th>
											</tr>
										</thead>
										<tbody class="buscar text-uppercase">
											<?php
											foreach ($documentos_proveedor as $documento) {
												$id_documento = $documento['id'];
												$nombre       = $documento['nom_documento'];
												$activo       = $documento['activo'];
												$descargar    = $documento['nombre'];

												$ver = ($activo == 1) ? '' : 'd-none';
												?>
												<tr class="text-center text-dark <?=$ver?>">
													<td><?=$nombre?></td>
													<td>
														<a class="btn btn-success btn-sm" href="<?=PUBLIC_PATH?>upload/<?=$descargar?>" download data-tooltip="tooltip" data-placement="bottom" title="Descargar archivo">
															<i class="fa fa-download"></i>
														</a>
													</td>
													<!-- <td>
														<button type="button" class="btn btn-primary btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Subir">
															<i class="fa fa-upload"></i>
														</button>
													</td> -->
												</tr>
												<?php
											}
											?>
										</tbody>
									</table>
								</div>
							</div>


							<div class="col-lg-12 form-group mt-4">
								<div class="table-responsive">
									<table class="table table-hover border" width="100%" cellspacing="0">
										<thead>
											<tr class="text-center font-weight-bold">
												<th colspan="3">
													EVALUACIONES REALIZADAS
												</th>
											</tr>
											<tr class="text-center font-weight-bold">
												<th>Año evaluado</th>
												<th>Calificacion total</th>
											</tr>
										</thead>
										<tbody class="buscar text-uppercase">
											<?php
											foreach ($calificacion_proveedor as $calificacion) {
												$id_calificacion = $calificacion['id'];
												$anio_evaluado   = date('Y', strtotime($calificacion['fecha_evaluacion']));
												$total           = $calificacion['total'];

												$badge = '';

												if ($total >= 4.5 && $total == 5.0) {
													$badge = '<span class="badge badge-primary p-2">Muy confiable</span>';
												}

												if ($total >= 4.0 && $total <= 4.4) {
													$badge = '<span class="badge badge-success p-2">Confiable</span>';
												}

												if ($total >= 3.6 && $total <= 3.9) {
													$badge = '<span class="badge badge-warning p-2">Aceptable</span>';
												}

												if ($total <= 3.5) {
													$badge = '<span class="badge badge-danger p-2">No confiable</span>';
												}

												?>
												<tr class="text-center text-dark">
													<td><?=$anio_evaluado?></td>
													<td><?=$total?></td>
													<td><?=$badge?></td>
												</tr>
												<?php
											}
											?>
										</tbody>
									</table>
								</div>
							</div>


							<div class="form-group col-lg-12">
								<a href="<?=BASE_URL?>imprimir/hoja_registro?proveedor=<?=base64_encode($id_proveedor)?>" target="_blank" class="btn btn-secondary btn-sm float-left">
									<i class="fa fa-print"></i>
									&nbsp;
									Imprimir
								</a>
								<button type="submit" class="btn btn-success btn-sm float-right">
									<i class="fa fa-save"></i>
									&nbsp;
									Guardar
								</button>
							</div>


						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="agregar_contacto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Agregar contacto</h5>
				</div>
				<form method="POST">
					<input type="hidden" name="id_log" value="<?=$id_log?>">
					<input type="hidden" name="id_proveedor" value="<?=$id_proveedor?>">
					<div class="modal-body border-0">
						<div class="row p-2">
							<div class="form-group col-lg-12">
								<label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="nombre_contacto" required>
							</div>
							<div class="form-group col-lg-12">
								<label class="font-weight-bold">Telefono</label>
								<input type="text" class="form-control" name="telefono_contacto">
							</div>
							<div class="form-group col-lg-12">
								<label class="font-weight-bold">Correo electronico <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="correo_contacto" required>
							</div>
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold">Cargo/Area <span class="text-danger">*</span></label>
								<select class="form-control" name="cargo_contacto" required>
									<option value="" selected="">Seleccione una opcion...</option>
									<option value="Asesor">Asesor</option>
									<option value="Contacto">Contacto</option>
									<option value="Compras/Finanzas">Compras/Finanzas</option>
								</select>
							</div>
						</div>
					</div>
					<div class="modal-footer border-0">
						<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
							<i class="fa fa-times"></i>
							&nbsp;
							Cerrar
						</button>
						<button type="submit" class="btn btn-success btn-sm">
							<i class="fa fa-save"></i>
							&nbsp;
							Guardar
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>



	<!-- Modal -->
	<div class="modal fade" id="agregar_banco" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Agregar banco</h5>
				</div>
				<form method="POST">
					<input type="hidden" name="id_log" value="<?=$id_log?>">
					<input type="hidden" name="id_proveedor" value="<?=$id_proveedor?>">
					<div class="modal-body border-0">
						<div class="row p-2">
							<div class="form-group col-lg-12">
								<label class="font-weight-bold">Nombre del banco <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="nom_banco" required>
							</div>
							<div class="form-group col-lg-12">
								<label class="font-weight-bold">Numero de la cuenta bancaria <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="num_banco" required>
							</div>
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold">Tipo de cuenta <span class="text-danger">*</span></label>
								<select class="form-control" name="tipo_cuenta" required>
									<option value="" selected>Seleccione una opcion...</option>
									<option value="Ahorros">Ahorros</option>
									<option value="Corriente">Corriente</option>
								</select>
							</div>
						</div>
					</div>
					<div class="modal-footer border-0">
						<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
							<i class="fa fa-times"></i>
							&nbsp;
							Cerrar
						</button>
						<button type="submit" class="btn btn-success btn-sm">
							<i class="fa fa-save"></i>
							&nbsp;
							Guardar
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<?php
	include_once VISTA_PATH . 'script_and_final.php';

	if (isset($_POST['nombre'])) {
		$instancia->actualizarProveedorControl();
	}

	if (isset($_POST['nombre_contacto'])) {
		$instancia->agregarContactoControl();
	}

	if (isset($_POST['nom_banco'])) {
		$instancia->agregarBancoControl();
	}
}
?>
<script type="text/javascript" src="<?=PUBLIC_PATH?>js/proveedor/funcionesProveedor.js"></script>