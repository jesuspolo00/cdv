<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloInventario extends conexion
{

    public static function agregarArticuloModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
        descripcion,
        marca,
        modelo,
        fecha_compra,
        codigo,
        id_user,
        id_area,
        id_categoria,
        user_log,
        estado
        )
        VALUES
        (
        '" . $datos['descripcion'] . "',
        '" . $datos['marca'] . "',
        '" . $datos['modelo'] . "',
        '" . $datos['fecha_compra'] . "',
        '" . $datos['codigo'] . "',
        '" . $datos['id_user'] . "',
        '" . $datos['id_area'] . "',
        '" . $datos['id_categoria'] . "',
        '" . $datos['id_log'] . "',
        '" . $datos['estado'] . "'
        );
        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => true, 'id' => $id);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function agregarHojaVidaArticuloModel($datos)
    {
        $tabla  = 'hoja_vida';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_inventario, proveedor, frecuencia_mantenimiento, fecha_garantia, contacto_garantia, user_log)
        VALUES (:id, :pr, :fm, :fg, :cg, :idl)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_inventario']);
            $preparado->bindParam(':pr', $datos['proveedor']);
            $preparado->bindParam(':fm', $datos['frecuencia_mantenimiento']);
            $preparado->bindParam(':fg', $datos['fecha_garantia']);
            $preparado->bindParam(':cg', $datos['contacto_garantia']);
            $preparado->bindParam(':idl', $datos['user_log']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function verificarCodigoModel($codigo)
    {
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT codigo FROM inventario WHERE codigo = :c;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':c', $codigo);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarArticulosModel($consulta)
    {
        $tabla  = "inventario";
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT i.*,
        COUNT(id) AS cantidad,
        (SELECT a.nombre FROM areas a WHERE a.id = i.id_area) AS nom_area,
        (SELECT c.nombre FROM  categoria c WHERE c.id = i.id_categoria) AS nom_categoria,
        (SELECT c.hoja_vida FROM  categoria c WHERE c.id = i.id_categoria) AS hoja_vida,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = i.id_user) AS nom_user,
        (SELECT e.nombre FROM estado e WHERE e.id = i.estado) AS nom_estado FROM " . $tabla . " i " . $consulta . " GROUP BY i.descripcion";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarArticulosPanelModel($consulta)
    {
        $tabla  = "inventario";
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT i.*,
        (SELECT a.nombre FROM areas a WHERE a.id = i.id_area) AS nom_area,
        (SELECT c.nombre FROM  categoria c WHERE c.id = i.id_categoria) AS nom_categoria,
        (SELECT c.hoja_vida FROM  categoria c WHERE c.id = i.id_categoria) AS hoja_vida,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = i.id_user) AS nom_user,
        (SELECT e.nombre FROM estado e WHERE e.id = i.estado) AS nom_estado FROM " . $tabla . " i " . $consulta;
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarArticulosCantidadPanelModel($consulta)
    {
        $tabla  = "inventario";
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        i.*,
        COUNT(id) AS cantidad,
        (SELECT a.nombre FROM areas a WHERE a.id = i.id_area) AS nom_area,
        (SELECT c.nombre FROM categoria c WHERE c.id = i.id_categoria) AS nom_categoria
        FROM inventario i " . $consulta . " GROUP BY i.descripcion, i.id_area, i.id_categoria ORDER BY i.descripcion;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTodosArticulosModel()
    {
        $tabla  = "inventario";
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT i.*,
        (SELECT a.nombre FROM areas a WHERE a.id = i.id_area) AS nom_area,
        (SELECT c.nombre FROM  categoria c WHERE c.id = i.id_categoria) AS nom_categoria,
        (SELECT c.hoja_vida FROM  categoria c WHERE c.id = i.id_categoria) AS hoja_vida,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = i.id_user) AS nom_user,
        (SELECT e.nombre FROM estado e WHERE e.id = i.estado) AS nom_estado FROM " . $tabla . " i ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarCantidadesArticulosModel()
    {
        $tabla  = "inventario";
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        i.*,
        COUNT(id) AS cantidad,
        (SELECT a.nombre FROM areas a WHERE a.id = i.id_area) AS nom_area,
        (SELECT c.nombre FROM categoria c WHERE c.id = i.id_categoria) AS nom_categoria
        FROM inventario i GROUP BY i.descripcion, i.id_area, i.id_categoria ORDER BY i.descripcion;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function hojaVidaArticuloModel($id)
    {
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        i.id AS id_inventario,
        i.*,
        h.*,
        h.id AS id_hoja,
        h.fechareg AS fecha_hoja,
        (SELECT e.nombre FROM estado e WHERE e.id = i.estado) AS nom_estado,
        (SELECT a.nombre FROM areas a WHERE a.id = i.id_area) AS nom_area,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = h.proveedor) AS nom_proveedor,
        (SELECT u.documento FROM usuarios u WHERE u.id_user = h.proveedor) AS num_proveedor,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = i.id_user) AS usuario
        FROM inventario i
        LEFT JOIN hoja_vida h ON h.id_inventario = i.id
        WHERE i.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarEstadosModel()
    {
        $tabla  = 'estado';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla;
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarHojaVidaModel($datos)
    {
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE inventario SET
        descripcion = '" . $datos['descripcion'] . "',
        marca = '" . $datos['marca'] . "',
        modelo = '" . $datos['modelo'] . "',
        id_area = '" . $datos['area'] . "',
        fecha_compra = '" . $datos['fecha_compra'] . "',
        user_log = '" . $datos['id_log'] . "'
        WHERE id = '" . $datos['id_inventario'] . "'
        ;

        UPDATE hoja_vida SET
        proveedor = '" . $datos['proveedor'] . "',
        frecuencia_mantenimiento = '" . $datos['frecuencia_mantenimiento'] . "',
        fecha_garantia = '" . $datos['fecha_garantia'] . "',
        contacto_garantia = '" . $datos['contacto_garantia'] . "',
        user_log = '" . $datos['id_log'] . "',
        fecha_update = NOW()
        WHERE id = '" . $datos['id_hoja'] . "'
        ;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function reportarArticuloModel($datos)
    {
        $tabla  = 'reporte';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO reporte (id_inventario, id_user, tipo_reporte, observacion, user_log, fecha_reporte)
        VALUES (:inv, :idu, :tr, :ob, :ul, :fr)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':inv', $datos['id_articulo']);
            $preparado->bindParam(':idu', $datos['id_usuario']);
            $preparado->bindParam(':tr', $datos['estado']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':ul', $datos['id_log']);
            $preparado->bindParam(':fr', $datos['fecha_reporte']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarEstadoArticuloModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = :e WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_articulo']);
            $preparado->bindParam(':e', $datos['estado']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTodosReportesModel()
    {
        $tabla  = 'reporte';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        iv.id AS id_inventario,
        iv.*,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
        (SELECT r.id FROM reporte r WHERE r.id_inventario = iv.id ORDER BY r.id DESC LIMIT 1) AS id_reporte,
        (SELECT r.fechareg FROM reporte r WHERE r.id_inventario = iv.id ORDER BY r.id DESC LIMIT 1) AS fecha_reportado,
        (SELECT r.observacion FROM reporte r WHERE r.id_inventario = iv.id ORDER BY r.id DESC LIMIT 1) AS observacion,
        (SELECT e.nombre FROM estado e WHERE e.id = iv.estado) AS nom_estado,
        (SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS nom_area
        FROM inventario iv WHERE iv.estado IN(2,3) ORDER BY iv.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function reportesMantenimientoModel()
    {
        $tabla  = 'reporte';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        iv.id AS id_inventario,
        iv.*,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
        (SELECT r.id FROM reporte r WHERE r.id_inventario = iv.id ORDER BY r.id DESC LIMIT 1) AS id_reporte,
        (SELECT r.fechareg FROM reporte r WHERE r.id_inventario = iv.id ORDER BY r.id DESC LIMIT 1) AS fecha_reportado,
        (SELECT r.observacion FROM reporte r WHERE r.id_inventario = iv.id ORDER BY r.id DESC LIMIT 1) AS observacion,
        (SELECT e.nombre FROM estado e WHERE e.id = iv.estado) AS nom_estado,
        (SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS nom_area
        FROM inventario iv WHERE iv.estado = 3;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function solucionarReporteModel($datos)
    {
        $tabla  = 'reporte';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO reporte (id_inventario, id_user, tipo_reporte, observacion, user_log, id_respuesta, fecha_reporte)
        VALUES (:inv, :idu, :tr, :ob, :ul, :idr, :fr)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':inv', $datos['id_articulo']);
            $preparado->bindParam(':idu', $datos['id_usuario']);
            $preparado->bindParam(':tr', $datos['estado']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':idr', $datos['id_reporte']);
            $preparado->bindParam(':ul', $datos['id_log']);
            $preparado->bindParam(':fr', $datos['fecha_reporte']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarReportesModel($id)
    {
        $tabla  = 'reporte';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        i.id AS id_inventario,
        r.id AS id_reporte,
        r.observacion,
        r.fecha_reporte,
        r.id_respuesta,
        i.*,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = i.id_user) AS usuario,
        (SELECT e.nombre FROM estado e WHERE e.id = r.tipo_reporte) AS nom_estado,
        (SELECT a.nombre FROM areas a WHERE a.id = i.id_area) AS nom_area
        FROM reporte r
        LEFT JOIN inventario i ON i.id = r.id_inventario
        WHERE r.id_inventario = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarFechaReportadoModel($id)
    {
        $tabla  = 'reporte';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT rp.* FROM " . $tabla . " rp WHERE rp.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function listadoArticulosModel($id)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        iv.id AS id_inventario,
        iv.*,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
        (SELECT r.id FROM reporte r WHERE r.id_inventario = iv.id ORDER BY r.id DESC LIMIT 1) AS id_reporte,
        (SELECT r.fecha_reporte FROM reporte r WHERE r.id_inventario = iv.id ORDER BY r.id DESC LIMIT 1) AS fecha_reporte,
        (SELECT r.observacion FROM reporte r WHERE r.id_inventario = iv.id ORDER BY r.id DESC LIMIT 1) AS observacion,
        (SELECT e.nombre FROM estado e WHERE e.id = iv.estado) AS nom_estado,
        (SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS nom_area,
        (SELECT c.nombre FROM categoria c WHERE c.id = iv.id_categoria) AS nom_categoria,
        (SELECT c.hoja_vida FROM categoria c WHERE c.id = iv.id_categoria) AS hoja_vida
        FROM inventario iv WHERE iv.id_user = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function liberarArticuloModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET id_area = :ida, id_user = :idu, estado = :e, user_log = :idl WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $datos['id_articulo']);
            $preparado->bindValue(':ida', $datos['id_area']);
            $preparado->bindValue(':idu', $datos['id_user']);
            $preparado->bindValue(':e', $datos['estado']);
            $preparado->bindValue(':idl', $datos['id_log']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function reasignarArticuloModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET id_area = :ida, id_user = :idu, estado = :e, user_log = :idl WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $datos['id_articulo']);
            $preparado->bindValue(':ida', $datos['id_area']);
            $preparado->bindValue(':idu', $datos['id_user']);
            $preparado->bindValue(':e', $datos['estado']);
            $preparado->bindValue(':idl', $datos['id_log']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function agregarHardwareModel($datos)
    {
        $tabla  = 'hardware';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_inventario, id_componente, user_log) VALUES (:idv, :idc, :idl);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':idv', $datos['id_inventario']);
            $preparado->bindValue(':idc', $datos['id_componente']);
            $preparado->bindValue(':idl', $datos['id_log']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarHardwareInventarioModel($id)
    {
        $tabla  = 'hardware';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT i.* FROM " . $tabla . " h
        LEFT JOIN inventario i ON i.id = h.id_componente
        WHERE h.id_inventario = :id AND i.id_categoria = 2 AND h.activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarComponentesInventarioModel()
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT i.*,
        IF((SELECT h.id FROM hardware h WHERE h.id_componente = i.id AND h.activo = 1) IS NULL, 'no', 'si') AS asignado,
        (SELECT a.nombre FROM areas a WHERE a.id = i.id_area) AS nom_area,
        (SELECT a.nombre FROM categoria a WHERE a.id = i.id_categoria) AS nom_categoria
        FROM " . $tabla . " i WHERE i.id_categoria = 2 AND i.estado IN(1,4,7);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarSoftwareInventarioModel($id)
    {
        $tabla  = 'software';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_inventario = :id AND activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function liberarHardwareModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE hardware SET activo = 0, user_log = :idl WHERE id_inventario = :idv AND id_componente = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':id', $datos['id_componente']);
            $preparado->bindParam(':idv', $datos['id_inventario']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function cantidadesAreaModel($id)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        IF(estado IN(1,4,7), COUNT(id), 0) AS cantidad_activo,
        IF(estado IN(5), COUNT(id), 0) AS cantidad_descontinuado,
        IF(estado IN(3), COUNT(id), 0) AS cantidad_mantenimiento,
        IF(estado IN(6), COUNT(id), 0) AS cantidad_liberado
        FROM inventario i WHERE i.id_area = :id GROUP BY i.estado;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function cantidadesGeneralModel()
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        IF(estado IN(1,4,7), COUNT(id), 0) AS cantidad_activo,
        IF(estado IN(5), COUNT(id), 0) AS cantidad_descontinuado,
        IF(estado IN(3), COUNT(id), 0) AS cantidad_mantenimiento,
        IF(estado IN(6), COUNT(id), 0) AS cantidad_liberado
        FROM inventario i GROUP BY i.estado;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function agregarSoftwareModel($datos)
    {
        $tabla  = 'software';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_inventario, descripcion, fabricante, version, licencia, user_log) VALUES (:idv, :d, :f, :v, :l, :idl);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':idv', $datos['id_inventario']);
            $preparado->bindParam(':d', $datos['descripcion_soft']);
            $preparado->bindParam(':v', $datos['version_soft']);
            $preparado->bindParam(':f', $datos['fabricante_soft']);
            $preparado->bindParam(':l', $datos['licencia_soft']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
