<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloSolicitud extends conexion
{

    public static function registrarSolicitudModel($datos)
    {
        $tabla  = 'solicitudes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
        id_user,
        id_area,
        fecha_solicitud,
        id_log,
        justificacion,
        anio,
        incremental_anio
        )
        VALUES (:idu, :ida, :fs, :idl, :j, :anio, :inc_anio);

        INSERT INTO solicitudes_inicial (
        id_user,
        id_area,
        fecha_solicitud,
        id_log,
        justificacion,
        anio,
        incremental_anio
        )
        VALUES (:idu, :ida, :fs, :idl, :j, :anio, :inc_anio);
        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['area']);
            $preparado->bindParam(':fs', $datos['fecha_solicitud']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':j', $datos['justificacion']);
            $preparado->bindParam(':anio', $datos['anio']);
            $preparado->bindParam(':inc_anio', $datos['incremental_anio']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => true, 'id' => $id);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function registrarProdcuctosModel($datos)
    {
        $tabla  = 'solicitud_productos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
        id_solicitud,
        producto,
        cantidad,
        id_log,
        iva
        )
        VALUES (:ids,:p,:c,:idl,0);

        INSERT INTO solicitud_productos_inicial (
        id_solicitud,
        producto,
        cantidad,
        id_log,
        iva
        )
        VALUES (:ids,:p,:c,:idl,0);
        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ids', $datos['id_solicitud']);
            $preparado->bindParam(':p', $datos['producto']);
            $preparado->bindParam(':c', $datos['cantidad']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => true, 'id' => $id);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarSolicitudesModel()
    {
        $tabla  = 'solicitudes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        s.*,
        (SELECT a.nombre FROM areas a WHERE a.id = s.id_area) AS area_nom,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = s.id_user) AS nom_usuario
        FROM solicitudes s ORDER BY anio DESC, ABS(incremental_anio) DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarSolicitudModel($datos)
    {
        $tabla  = 'solicitudes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        s.*,
        (SELECT a.nombre FROM areas a WHERE a.id = s.id_area) AS area_nom,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = s.id_user) AS nom_usuario
        FROM solicitudes s
        LEFT JOIN usuarios u ON u.id_user = s.id_user
        LEFT JOIN areas a ON a.id = s.id_area
        WHERE CONCAT(s.fecha_solicitud, ' ', a.nombre, ' ', s.incremental_anio) LIKE '%" . $datos['buscar'] . "%'
        " . $datos['cdv'] . "
        " . $datos['anio'] . "
        ORDER BY id DESC, anio ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarSolicitudesUsuarioModel($id)
    {
        $tabla  = 'solicitudes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        s.*,
        (SELECT a.nombre FROM areas a WHERE a.id = s.id_area) AS area_nom,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = s.id_user) AS nom_usuario
        FROM solicitudes s WHERE s.id_user = :id ORDER BY fechareg DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosSolicitudIdModel($id)
    {
        $tabla  = 'solicitudes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        s.*,
        (SELECT a.nombre FROM areas a WHERE a.id = s.id_area) AS area_nom,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = s.id_user) AS nom_usuario,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = s.id_log) AS nom_aprobado,
        (SELECT u.documento FROM usuarios u WHERE u.id_user = s.id_user) AS documento,
        (SELECT u.telefono FROM usuarios u WHERE u.id_user = s.id_user) AS telefono,
        (SELECT p.nombre FROM proveedor_detalle p WHERE p.id = s.id_proveedor) AS nom_proveedor,
        ac.firmas
        FROM solicitudes s
        LEFT JOIN anio_cdv ac ON ac.nombre = s.anio
        WHERE s.id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosSolicitudInicialIdModel($id)
    {
        $tabla  = 'solicitudes_inicial';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        s.*,
        (SELECT a.nombre FROM areas a WHERE a.id = s.id_area) AS area_nom,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = s.id_user) AS nom_usuario,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = s.id_log) AS nom_aprobado,
        (SELECT u.documento FROM usuarios u WHERE u.id_user = s.id_user) AS documento,
        (SELECT u.telefono FROM usuarios u WHERE u.id_user = s.id_user) AS telefono,
        ac.firmas
        FROM solicitudes s
        LEFT JOIN anio_cdv ac ON ac.nombre = s.anio
        WHERE s.id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarProdcutosSolicitudModel($id)
    {
        $tabla  = 'solicitud_productos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " sp WHERE sp.id_solicitud = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarProdcutosSolicitudInicialModel($id)
    {
        $tabla  = 'solicitud_productos_inicial';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " sp WHERE sp.id_solicitud = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarEstadoModel($datos)
    {
        $tabla  = 'solicitudes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = :e, fecha_aplazado = :f, observacion = :o, id_log = :idl, iva = :iva, id_proveedor = :idp, fecha_solicitud = :fs, justificacion = :jf, id_edit = :ide, fecha_edit = NOW() WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_solicitud']);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':f', $datos['fecha_aplazado']);
            $preparado->bindParam(':o', $datos['observacion']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':iva', $datos['iva']);
            $preparado->bindParam(':idp', $datos['id_proveedor']);
            $preparado->bindParam(':fs', $datos['fecha_solicitado']);
            $preparado->bindParam(':jf', $datos['justificacion']);
            $preparado->bindParam(':ide', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function preciosProductoControl($datos)
    {
        $tabla  = 'solicitud_productos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET precio = :p, producto = :n, cantidad = :c, iva = :iv WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_producto']);
            $preparado->bindParam(':p', $datos['precio']);
            $preparado->bindParam(':n', $datos['nom_producto']);
            $preparado->bindParam(':c', $datos['cantidad']);
            $preparado->bindParam(':iv', $datos['iva']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function verificarSolicitudModel($datos)
    {
        $tabla  = 'solicitud_verificacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
        id_solicitud,
        cantidad,
        observacion_cant,
        calidad,
        observacion_calidad,
        precios,
        observacion_precios,
        plazos,
        observacion_plazo,
        id_log,
        fecha_verificacion
        )
        VALUES
        (
        '" . $datos['id_solicitud'] . "',
        '" . $datos['cantidad'] . "',
        '" . $datos['observacion_cant'] . "',
        '" . $datos['calidad'] . "',
        '" . $datos['observacion_calidad'] . "',
        '" . $datos['precios'] . "',
        '" . $datos['observacion_precio'] . "',
        '" . $datos['plazos'] . "',
        '" . $datos['observacion_plazo'] . "',
        '" . $datos['id_log'] . "',
        '" . $datos['fecha_verificacion'] . "'
        );

        INSERT INTO solicitud_verificacion_inicial (
        id_solicitud,
        cantidad,
        observacion_cant,
        calidad,
        observacion_calidad,
        precios,
        observacion_precios,
        plazos,
        observacion_plazo,
        id_log,
        fecha_verificacion
        )
        VALUES
        (
        '" . $datos['id_solicitud'] . "',
        '" . $datos['cantidad'] . "',
        '" . $datos['observacion_cant'] . "',
        '" . $datos['calidad'] . "',
        '" . $datos['observacion_calidad'] . "',
        '" . $datos['precios'] . "',
        '" . $datos['observacion_precio'] . "',
        '" . $datos['plazos'] . "',
        '" . $datos['observacion_plazo'] . "',
        '" . $datos['id_log'] . "',
        '" . $datos['fecha_verificacion'] . "'
        );
        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosVerificacionModel($id)
    {
        $tabla  = 'solicitud_verificacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT *, (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = id_log) AS nom_usuario FROM " . $tabla . " WHERE id_solicitud = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosVerificacionInicialModel($id)
    {
        $tabla  = 'solicitud_verificacion_inicial';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT *, (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = id_log) AS nom_usuario FROM " . $tabla . " WHERE id_solicitud = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function removerProductoModel($id)
    {
        $tabla  = 'solicitud_productos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function anularSolicitudModel($datos)
    {
        $tabla  = 'solicitudes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET motivo = :m, activo = :e, id_log = :idl WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_solicitud']);
            $preparado->bindParam(':m', $datos['motivo']);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarAniosSolicitudModel()
    {
        $tabla  = 'solicitudes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM anio_cdv GROUP BY nombre ORDER BY id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function ultimoAnioIdSolicitudModel($anio)
    {
        $tabla  = 'solicitudes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT incremental_anio FROM  solicitudes WHERE anio = '" . $anio . "' ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
