<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloPermisos extends conexion
{

    public static function mostrarDatosPrefacturaModel($perfil, $opcion)
    {
        $tabla  = 'inv_permisos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_perfil = :p AND activo = 1 AND id_opcion = :o ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':p', $perfil);
            $preparado->bindParam(':o', $opcion);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarOpcionesPermisosModel()
    {
        $tabla  = 'inv_opcion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE activo = 1 ORDER BY nombre ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function opcionsIdActivosPerfilModel($perfil, $id_opcion)
    {
        $tabla  = 'inv_permisos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_perfil = :id AND id_opcion = :io AND activo = 1 ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $perfil);
            $preparado->bindParam(':io', $id_opcion);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function activarPermisoModel($datos)
    {
        $tabla  = 'inv_permisos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_opcion, id_perfil, user_log) VALUES (:o, :p, :ul);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':o', $datos['opcion']);
            $preparado->bindValue(':p', $datos['perfil']);
            $preparado->bindParam(':ul', $datos['user']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function inactivarPermisoModel($datos)
    {
        $tabla  = 'inv_permisos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 0 WHERE id_opcion = :o AND id_perfil = :p AND user_log = :ul AND activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':o', $datos['opcion']);
            $preparado->bindValue(':p', $datos['perfil']);
            $preparado->bindValue(':ul', $datos['user']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
