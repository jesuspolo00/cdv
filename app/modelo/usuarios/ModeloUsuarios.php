<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloUsuario extends conexion
{

    public static function mostrarUsuariosModel()
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT u.*,
        (SELECT p.nombre FROM perfiles p WHERE p.id = u.perfil) AS nom_perfil
        FROM " . $tabla . " u;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function registrarUsuarioModel($datos)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (documento, nombre, apellido, correo, telefono, user, pass, perfil, user_log) VALUES
        (:d, :n, :a, :c, :t, :u, :p, :pr, :idl)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(":d", $datos['documento']);
            $preparado->bindParam(":n", $datos['nombre']);
            $preparado->bindParam(":a", $datos['apellido']);
            $preparado->bindParam(":c", $datos['correo']);
            $preparado->bindParam(":t", $datos['telefono']);
            $preparado->bindParam(":u", $datos['usuario']);
            $preparado->bindParam(":p", $datos['pass']);
            $preparado->bindParam(":pr", $datos['perfil']);
            $preparado->bindParam(":idl", $datos['user_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => true, 'id' => $id);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function validarUsuarioModel($usuario)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE user = '" . $usuario . "' ORDER BY id_user DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function inactivarUsuarioModel($id, $log)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 0, user_log = :l, fecha_inactivo = NOW() WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':l', $log);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function activarUsuarioModel($id, $log)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 1, user_log = :l, fecha_activo = NOW() WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':l', $log);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarUsuarioModel($datos)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET documento = :d, nombre = :n, apellido = :ap, telefono = :t, correo = :c, perfil = :p, user_log = :idl WHERE id_user = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_user']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':d', $datos['documento']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':ap', $datos['apellido']);
            $preparado->bindParam(':t', $datos['telefono']);
            $preparado->bindParam(':c', $datos['correo']);
            $preparado->bindParam(':p', $datos['perfil']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function restablecerPassModel($id, $pass)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET pass = :p WHERE id_user = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':p', $pass);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
