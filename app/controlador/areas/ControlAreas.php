<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'areas' . DS . 'ModeloAreas.php';

class ControlAreas
{

    private static $instancia;

    public static function singleton_areas()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarAreasControl()
    {
        $mostrar = ModeloAreas::mostrarAreasModel();
        return $mostrar;
    }

    public function agregarAreasControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['nombre']) &&
            !empty($_POST['nombre'])
        ) {
            $datos = array(
                'id_log' => $_POST['id_log'],
                'nombre' => $_POST['nombre'],
            );

            $guardar = ModeloAreas::agregarAreasModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function activarAreaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {
            $activar = ModeloAreas::activarAreaModel($_POST['id']);
            return $activar;
        }
    }

    public function inactivarAreaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {
            $activar = ModeloAreas::inactivarAreaModel($_POST['id']);
            return $activar;
        }
    }
}
