<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'usuarios' . DS . 'ModeloUsuarios.php';
require_once CONTROL_PATH . 'hash.php';

class ControlUsuario
{

    private static $instancia;

    public static function singleton_usuario()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarUsuariosControl()
    {
        $mostrar = ModeloUsuario::mostrarUsuariosModel();
        return $mostrar;
    }

    public function registrarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['usuario']) &&
            !empty($_POST['usuario'])
        ) {

            if ($_POST['conf_password'] == $_POST['password']) {

                $pass = Hash::hashpass($_POST['conf_password']);

                $datos = array(
                    'documento' => $_POST['documento'],
                    'nombre'    => $_POST['nombre'],
                    'apellido'  => $_POST['apellido'],
                    'telefono'  => $_POST['telefono'],
                    'correo'    => $_POST['correo'],
                    'usuario'   => $_POST['usuario'],
                    'perfil'    => $_POST['perfil'],
                    'pass'      => $pass,
                    'user_log'  => $_POST['id_log'],
                );

                $guardar = ModeloUsuario::registrarUsuarioModel($datos);

                if ($guardar['guardar'] == true) {
                    echo '
                    <script>
                    ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                    setTimeout(recargarPagina,1050);

                    function recargarPagina(){
                        window.location.replace("index");
                    }
                    </script>
                    ';
                } else {
                    echo '
                    <script>
                    ohSnap("Ha ocurrido un error", {color: "red"});
                    </script>
                    ';
                }

            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }

        }
    }

    public function actualizarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user'])
        ) {
            $datos = array(
                'id_log'    => $_POST['id_log'],
                'id_user'   => $_POST['id_user'],
                'documento' => $_POST['documento_edit'],
                'nombre'    => $_POST['nombre_edit'],
                'apellido'  => $_POST['apellido_edit'],
                'telefono'  => $_POST['telefono_edit'],
                'correo'    => $_POST['correo_edit'],
                'perfil'    => $_POST['perfil_edit'],
            );

            $actualizar = ModeloUsuario::actualizarUsuarioModel($datos);

            if ($actualizar == true) {
                echo '
                <script>
                ohSnap("Actualizado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function validarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['usuario']) &&
            !empty($_POST['usuario'])
        ) {
            $validar = ModeloUsuario::validarUsuarioModel($_POST['usuario']);
            return $validar;
        }
    }

    public function inactivarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id']) &&
            isset($_POST['log']) &&
            !empty($_POST['log'])
        ) {
            $validar = ModeloUsuario::inactivarUsuarioModel($_POST['id'], $_POST['log']);
            return $validar;
        }
    }

    public function activarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id']) &&
            isset($_POST['log']) &&
            !empty($_POST['log'])
        ) {
            $validar = ModeloUsuario::activarUsuarioModel($_POST['id'], $_POST['log']);
            return $validar;
        }
    }

    public function restablecerPassControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {

            $pass = Hash::hashpass('cdv123456@');

            $validar = ModeloUsuario::restablecerPassModel($_POST['id'], $pass);
            return $validar;
        }
    }
}
