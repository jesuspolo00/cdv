<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'inventario' . DS . 'ModeloInventario.php';
require_once MODELO_PATH . 'categorias' . DS . 'ModeloCategorias.php';

class ControlInventario
{

    private static $instancia;

    public static function singleton_inventario()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarEstadosControl()
    {
        $mostrar = ModeloInventario::mostrarEstadosModel();
        return $mostrar;
    }

    public function hojaVidaArticuloControl($id)
    {
        $mostrar = ModeloInventario::hojaVidaArticuloModel($id);
        return $mostrar;
    }

    public function mostrarReportesControl($id)
    {
        $mostrar = ModeloInventario::mostrarReportesModel($id);
        return $mostrar;
    }

    public function mostrarTodosReportesControl()
    {
        $mostrar = ModeloInventario::mostrarTodosReportesModel();
        return $mostrar;
    }

    public function reportesMantenimientoControl()
    {
        $mostrar = ModeloInventario::reportesMantenimientoModel();
        return $mostrar;
    }

    public function mostrarFechaReportadoControl($id)
    {
        $mostrar = ModeloInventario::mostrarFechaReportadoModel($id);
        return $mostrar;
    }

    public function listadoArticulosControl($id)
    {
        $mostrar = ModeloInventario::listadoArticulosModel($id);
        return $mostrar;
    }

    public function mostrarHardwareInventarioControl($id)
    {
        $mostrar = ModeloInventario::mostrarHardwareInventarioModel($id);
        return $mostrar;
    }

    public function mostrarComponentesInventarioControl()
    {
        $mostrar = ModeloInventario::mostrarComponentesInventarioModel();
        return $mostrar;
    }

    public function mostrarSoftwareInventarioControl($id)
    {
        $mostrar = ModeloInventario::mostrarSoftwareInventarioModel($id);
        return $mostrar;
    }

    public function mostrarArticulosControl($area, $usuario)
    {

        if ($area != "") {
            $consulta = " WHERE i.id_area = " . $area;
        } else if ($usuario != "") {
            $consulta = " WHERE i.id_user = " . $usuario;
        } else if ($area != "" && $usuario != "") {
            $consulta = " WHERE i.id_user = " . $usuario . " AND i.id_area = " . $area;
        } else {
            $consulta = "";
        }

        $mostrar = ModeloInventario::mostrarArticulosModel($consulta);
        return $mostrar;
    }

    public function mostrarArticulosPanelControl($area, $usuario, $texto)
    {

        if ($area != "") {
            $consulta = " WHERE i.id_area = " . $area;
        } else if ($usuario != "") {
            $consulta = " WHERE i.id_user = " . $usuario;
        } else if ($area != "" && $usuario != "") {
            $consulta = " WHERE i.id_user = " . $usuario . " AND i.id_area = " . $area;
        } else if ($texto != "") {
            $consulta = " WHERE i.descripcion LIKE '%" . $texto . "%'";
        } else if ($area != "" && $usuario != "" && $texto != "") {
            $consulta = " WHERE i.id_user = " . $usuario . " AND i.id_area = " . $area . " AND i.descripcion LIKE '%" . $texto . "%'";
        } else {
            $consulta = "";
        }

        $mostrar = ModeloInventario::mostrarArticulosPanelModel($consulta);
        return $mostrar;
    }

    public function mostrarArticulosCantidadPanelControl($area, $usuario, $texto)
    {

        if ($area != "") {
            $consulta = " WHERE i.id_area = " . $area;
        } else if ($usuario != "") {
            $consulta = " WHERE i.id_user = " . $usuario;
        } else if ($area != "" && $usuario != "") {
            $consulta = " WHERE i.id_user = " . $usuario . " AND i.id_area = " . $area;
        } else if ($texto != "") {
            $consulta = " WHERE i.descripcion LIKE '%" . $texto . "%'";
        } else if ($area != "" && $usuario != "" && $texto != "") {
            $consulta = " WHERE i.id_user = " . $usuario . " AND i.id_area = " . $area . " AND i.descripcion LIKE '%" . $texto . "%'";
        } else {
            $consulta = "";
        }

        $mostrar = ModeloInventario::mostrarArticulosCantidadPanelModel($consulta);
        return $mostrar;
    }

    public function mostrarTodosArticulosControl()
    {
        $mostrar = ModeloInventario::mostrarTodosArticulosModel();
        return $mostrar;
    }

    public function mostrarCantidadesArticulosControl()
    {
        $mostrar = ModeloInventario::mostrarCantidadesArticulosModel();
        return $mostrar;
    }

    public function cantidadesAreaControl($id)
    {
        $mostrar = ModeloInventario::cantidadesAreaModel($id);
        return $mostrar;
    }

    public function cantidadesGeneralControl()
    {
        $mostrar = ModeloInventario::cantidadesGeneralModel();
        return $mostrar;
    }

    public function numeroAleatorio()
    {
        $numero = rand();

        $codigo = ModeloInventario::verificarCodigoModel($numero);

        if ($codigo['codigo'] != '') {
            $this->numeroAleatorio();
        } else {
            $num_codigo = $numero;
        }

        return $num_codigo;
    }

    public function agregarArticuloControl()
    {

        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['descripcion']) &&
            !empty($_POST['descripcion'])
        ) {

            $cantidad = $_POST['cantidad'];

            for ($i = 0; $i < $cantidad; $i++) {

                $codigo = $this->numeroAleatorio();

                $fecha_compra = ($_POST['fecha_compra'] == '') ? '0000-00-00' : $_POST['fecha_compra'];
                $fecha_vence  = ($_POST['fecha_vence'] == '') ? '0000-00-00' : $_POST['fecha_vence'];

                $datos = array(
                    'descripcion'  => $_POST['descripcion'],
                    'marca'        => $_POST['marca'],
                    'modelo'       => $_POST['modelo'],
                    'fecha_compra' => $fecha_compra,
                    'codigo'       => $codigo,
                    'id_user'      => 0,
                    'id_area'      => $_POST['area'],
                    'id_categoria' => $_POST['categoria'],
                    'estado'       => 1,
                    'id_log'       => $_POST['id_log'],
                );

                $guardar = ModeloInventario::agregarArticuloModel($datos);

                //$categoria_validate = ModeloCategorias::validarHojaVidaCategoriaModel($_POST['categoria']);

                if ($guardar['guardar'] == true) {
                    $datos_hoja = array(
                        'id_inventario'            => $guardar['id'],
                        'proveedor'                => $_POST['proveedor'],
                        'frecuencia_mantenimiento' => $_POST['frecuencia_man'],
                        'fecha_garantia'           => $fecha_vence,
                        'contacto_garantia'        => $_POST['contacto_garantia'],
                        'user_log'                 => $_POST['id_log'],
                    );

                    $guardar_hoja = ModeloInventario::agregarHojaVidaArticuloModel($datos_hoja);
                }

            }

            if ($guardar_hoja == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("inicio");
                }
                </script>
                ';

            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function actualizarHojaVidaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['descripcion']) &&
            !empty($_POST['descripcion'])
        ) {

            $fecha_compra   = ($_POST['fecha_compra'] == '') ? '0000-00-00' : $_POST['fecha_compra'];
            $fecha_garantia = ($_POST['fecha_garantia'] == '') ? '0000-00-00' : $_POST['fecha_garantia'];

            $datos_inventario = array(
                'id_log'                   => $_POST['id_log'],
                'id_inventario'            => $_POST['id_inventario'],
                'descripcion'              => $_POST['descripcion'],
                'marca'                    => $_POST['marca'],
                'modelo'                   => $_POST['modelo'],
                'area'                     => $_POST['area'],
                //'estado'                   => $_POST['estado'],
                'fecha_compra'             => $fecha_compra,
                'id_hoja'                  => $_POST['id_hoja'],
                'proveedor'                => $_POST['proveedor'],
                'frecuencia_mantenimiento' => $_POST['frecuencia_mantenimiento'],
                'fecha_garantia'           => $fecha_garantia,
                'contacto_garantia'        => $_POST['contacto_garantia'],
            );

            $actualizar = ModeloInventario::actualizarHojaVidaModel($datos_inventario);

            if ($actualizar == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("hojaVida?inventario=' . base64_encode($_POST['id_inventario']) . '");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function reportarArticuloControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $fecha_reporte = ($_POST['fecha_reporte'] == '') ? date('Y-m-d H:i:s') : $_POST['fecha_reporte'];

            $datos_reporte = array(
                'id_log'        => $_POST['id_log'],
                'id_usuario'    => $_POST['id_usuario'],
                'id_articulo'   => $_POST['id_articulo'],
                'estado'        => $_POST['estado'],
                'observacion'   => $_POST['observacion'],
                'fecha_reporte' => $fecha_reporte,
            );

            $datos_articulo = array(
                'id_articulo' => $_POST['id_articulo'],
                'estado'      => $_POST['estado'],
            );

            $actualizar_estado = ModeloInventario::actualizarEstadoArticuloModel($datos_articulo);

            if ($_POST['estado'] == 6) {
                $datos_liberar = array(
                    'id_articulo' => $_POST['id_articulo'],
                    'estado'      => $_POST['estado'],
                    'id_area'     => 0,
                    'id_user'     => 0,
                    'id_log'      => $_POST['id_log'],
                );

                $liberar = ModeloInventario::liberarArticuloModel($datos_liberar);
            }

            $guardar = ModeloInventario::reportarArticuloModel($datos_reporte);

            $url = '../inicio';

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Reportado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("' . $url . '");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function solucionarReporteControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_reporte']) &&
            !empty($_POST['id_reporte'])
        ) {

            $fecha_reporte = ($_POST['fecha_reporte'] == '') ? date('Y-m-d H:i:s') : $_POST['fecha_reporte'];

            $datos = array(
                'id_log'        => $_POST['id_log'],
                'id_usuario'    => $_POST['id_usuario'],
                'id_articulo'   => $_POST['id_articulo'],
                'estado'        => 4,
                'observacion'   => $_POST['observacion'],
                'id_reporte'    => $_POST['id_reporte'],
                'fecha_reporte' => $fecha_reporte,
            );

            $datos_articulo = array(
                'id_articulo' => $_POST['id_articulo'],
                'estado'      => 4,
            );

            $actualizar_estado = ModeloInventario::actualizarEstadoArticuloModel($datos_articulo);

            $solucion = ModeloInventario::solucionarReporteModel($datos);

            if ($solucion == true) {
                echo '
                <script>
                ohSnap("Reportado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function reasignarArticuloControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_articulo_reg']) &&
            !empty($_POST['id_articulo_reg'])
        ) {

            $fecha_reporte = ($_POST['fecha_reporte'] == '') ? date('Y-m-d H:i:s') : $_POST['fecha_reporte'];

            $datos_reporte = array(
                'id_log'        => $_POST['id_log'],
                'id_usuario'    => $_POST['id_log'],
                'id_articulo'   => $_POST['id_articulo_reg'],
                'estado'        => 7,
                'observacion'   => '',
                'fecha_reporte' => $fecha_reporte,
            );

            $guardar = ModeloInventario::reportarArticuloModel($datos_reporte);

            $datos = array(
                'id_log'      => $_POST['id_log'],
                'id_articulo' => $_POST['id_articulo_reg'],
                'id_user'     => $_POST['usuario_reasignar'],
                'id_area'     => $_POST['area_reasignar'],
                'estado'      => 7,
            );

            $reasignar = ModeloInventario::reasignarArticuloModel($datos);

            if ($reasignar == true) {
                echo '
                <script>
                ohSnap("Reportado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("../inicio");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function agregarHardwareControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_inventario']) &&
            !empty($_POST['id_inventario'])
        ) {

            $fecha_compra = ($_POST['fecha_compra'] == '') ? '0000-00-00' : $_POST['fecha_compra'];

            $codigo = $this->numeroAleatorio();

            $datos = array(
                'descripcion'  => $_POST['descripcion'],
                'marca'        => $_POST['marca'],
                'modelo'       => $_POST['modelo'],
                'fecha_compra' => $fecha_compra,
                'codigo'       => $codigo,
                'id_user'      => 0,
                'id_area'      => $_POST['area'],
                'id_categoria' => $_POST['categoria'],
                'estado'       => 1,
                'id_log'       => $_POST['id_user'],
            );

            $guardar = ModeloInventario::agregarArticuloModel($datos);

            if ($guardar['guardar'] == true) {
                $datos_hardware = array(
                    'id_inventario' => $_POST['id_inventario'],
                    'id_componente' => $guardar['id'],
                    'id_log'        => $_POST['id_user'],
                );

                $asignar_hardware = ModeloInventario::agregarHardwareModel($datos_hardware);

                if ($asignar_hardware == true) {
                    echo '
                    <script>
                    ohSnap("Asignado correctamente!", {color: "green", "duration": "1000"});
                    setTimeout(recargarPagina,1050);

                    function recargarPagina(){
                        window.location.replace("hojaVida?inventario=' . base64_encode($_POST['id_inventario']) . '");
                    }
                    </script>
                    ';
                } else {
                    echo '
                    <script>
                    ohSnap("Ha ocurrido un error", {color: "red"});
                    </script>
                    ';
                }
            }

        }
    }

    public function agregarSoftwareControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_inventario']) &&
            !empty($_POST['id_inventario'])
        ) {
            $datos = array(
                'id_log'           => $_POST['id_log'],
                'id_inventario'    => $_POST['id_inventario'],
                'descripcion_soft' => $_POST['descripcion_soft'],
                'version_soft'     => $_POST['version_soft'],
                'fabricante_soft'  => $_POST['fabricante_soft'],
                'licencia_soft'    => $_POST['licencia_soft'],
            );

            $guardar_soft = ModeloInventario::agregarSoftwareModel($datos);

            if ($guardar_soft == true) {
                echo '
                <script>
                ohSnap("Asignado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("hojaVida?inventario=' . base64_encode($_POST['id_inventario']) . '");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function asignarHardwareControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_inventario']) &&
            !empty($_POST['id_inventario'])
        ) {
            $datos_hardware = array(
                'id_inventario' => $_POST['id_inventario'],
                'id_componente' => $_POST['id'],
                'id_log'        => $_POST['id_log'],
            );

            $asignar_hardware = ModeloInventario::agregarHardwareModel($datos_hardware);
            return $asignar_hardware;
        }
    }

    public function liberarHardwareControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_inventario']) &&
            !empty($_POST['id_inventario'])
        ) {
            $datos_hardware = array(
                'id_inventario' => $_POST['id_inventario'],
                'id_componente' => $_POST['id'],
                'id_log'        => $_POST['id_log'],
            );

            $asignar_hardware = ModeloInventario::liberarHardwareModel($datos_hardware);
            return $asignar_hardware;
        }
    }
}
