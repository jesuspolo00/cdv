<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'permisos' . DS . 'ModeloPermisos.php';

class ControlPermisos
{

    private static $instancia;

    public static function singleton_permisos()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function consultarPermisosPerfilControl($perfil, $opcion)
    {
        $mostrar = ModeloPermisos::mostrarDatosPrefacturaModel($perfil, $opcion);
        return $mostrar;
    }

    public function mostrarOpcionesPermisosControl()
    {
        $mostrar = ModeloPermisos::mostrarOpcionesPermisosModel();
        return $mostrar;
    }

    public function opcionsIdActivosPerfilControl($id_perfil, $id_opcion)
    {
        $mostrar = ModeloPermisos::opcionsIdActivosPerfilModel($id_perfil, $id_opcion);
        return $mostrar;
    }

    public function activarPermisoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['opcion']) &&
            !empty($_POST['opcion']) &&
            isset($_POST['perfil']) &&
            !empty($_POST['perfil']) &&
            isset($_POST['user']) &&
            !empty($_POST['user'])
        ) {

            $datos = array(
                'opcion' => $_POST['opcion'],
                'perfil' => $_POST['perfil'],
                'user'   => $_POST['user'],
            );

            $activar = ModeloPermisos::activarPermisoModel($datos);
            return $activar;
        }
    }

    public function inactivarPermisoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['opcion']) &&
            !empty($_POST['opcion']) &&
            isset($_POST['perfil']) &&
            !empty($_POST['perfil']) &&
            isset($_POST['user']) &&
            !empty($_POST['user'])
        ) {

            $datos = array(
                'opcion' => $_POST['opcion'],
                'perfil' => $_POST['perfil'],
                'user'   => $_POST['user'],
            );

            $activar = ModeloPermisos::inactivarPermisoModel($datos);
            return $activar;
        }
    }
}
