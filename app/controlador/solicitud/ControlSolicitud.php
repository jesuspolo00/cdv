<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'solicitud' . DS . 'ModeloSolicitud.php';
require_once CONTROL_PATH . 'hash.php';

class ControlSolicitud
{

    private static $instancia;

    public static function singleton_solicitud()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarSolicitudesControl()
    {
        $mostrar = ModeloSolicitud::mostrarSolicitudesModel();
        return $mostrar;
    }

    public function buscarSolicitudControl($datos)
    {
        $cdv  = ($datos['cdv'] == '') ? '' : ' AND s.id_area = ' . $datos['cdv'];
        $anio = ($datos['anio'] == '') ? '' : " AND s.anio = '" . $datos['anio'] . "'";

        $datos = array('cdv' => $cdv, 'anio' => $anio, 'buscar' => $datos['buscar']);

        $mostrar = ModeloSolicitud::buscarSolicitudModel($datos);
        return $mostrar;
    }

    public function mostrarSolicitudesUsuarioControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarSolicitudesUsuarioModel($id);
        return $mostrar;
    }

    public function mostrarAniosSolicitudControl()
    {
        $mostrar = ModeloSolicitud::mostrarAniosSolicitudModel();
        return $mostrar;
    }

    public function mostrarDatosSolicitudIdControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarDatosSolicitudIdModel($id);
        return $mostrar;
    }

    public function mostrarProdcutosSolicitudControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarProdcutosSolicitudModel($id);
        return $mostrar;
    }

    public function mostrarDatosVerificacionControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarDatosVerificacionModel($id);
        return $mostrar;
    }

    //------------------------INICIAL------------------------------//

    public function mostrarDatosSolicitudInicialIdControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarDatosSolicitudInicialIdModel($id);
        return $mostrar;
    }

    public function mostrarProdcutosSolicitudInicialControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarProdcutosSolicitudInicialModel($id);
        return $mostrar;
    }

    public function mostrarDatosVerificacionInicialControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarDatosVerificacionInicialModel($id);
        return $mostrar;
    }

    public function registrarSolicitudControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $anio_fecha = date('Y', strtotime($_POST['fecha_solicitud']));

            $ultimo_id_anio   = ModeloSolicitud::ultimoAnioIdSolicitudModel($anio_fecha);
            $incremental_anio = ($ultimo_id_anio['incremental_anio'] == '') ? 1 : $ultimo_id_anio['incremental_anio'] + 1;

            $datos_solicitud = array(
                'id_log'           => $_POST['id_log'],
                'id_user'          => $_POST['id_user'],
                'fecha_solicitud'  => $_POST['fecha_solicitud'],
                'area'             => $_POST['area'],
                'justificacion'    => $_POST['justificacion'],
                'anio'             => $anio_fecha,
                'incremental_anio' => $incremental_anio,
            );

            $guardar = ModeloSolicitud::registrarSolicitudModel($datos_solicitud);

            if ($guardar['guardar'] == true) {

                $array_producto = array();
                $array_producto = $_POST['producto'];

                $array_cantidad = array();
                $array_cantidad = $_POST['cantidad'];

                $it = new MultipleIterator();
                $it->attachIterator(new ArrayIterator($array_producto));
                $it->attachIterator(new ArrayIterator($array_cantidad));

                foreach ($it as $a) {
                    $datos_producto = array(
                        'id_log'       => $_POST['id_log'],
                        'id_solicitud' => $guardar['id'],
                        'producto'     => $a[0],
                        'cantidad'     => $a[1],
                    );

                    $guardar_producto = ModeloSolicitud::registrarProdcuctosModel($datos_producto);
                }

                if ($guardar_producto['guardar'] == true) {
                    echo '
                    <script>
                    ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                    setTimeout(recargarPagina, 1050);

                    function recargarPagina()
                    {
                        window.location.replace("index");
                    }
                    </script>
                    ';
                } else {
                    echo '
                    <script>
                    ohSnap("Error de solicitud!", {color:"red", "duration":"1000"});
                    </script>
                    ';
                }
            }
        }
    }

    public function confirmarSolicitudControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $fecha_aplazado = ($_POST['fecha_aplazado'] == '') ? '0000-00-00' : $_POST['fecha_aplazado'];

            $anio_fecha = date('Y', strtotime($_POST['fecha_solicitado']));

            $url = ($_POST['url'] == 0) ? 'confirmar?solicitud=' . base64_encode($_POST['id_solicitud']) : 'editar?solicitud=' . base64_encode($_POST['id_solicitud']);

            $datos_solicitud = array(
                'id_solicitud'     => $_POST['id_solicitud'],
                'estado'           => $_POST['estado'],
                'fecha_solicitado' => $_POST['fecha_solicitado'],
                'fecha_aplazado'   => $fecha_aplazado,
                'observacion'      => $_POST['observacion'],
                'id_log'           => $_POST['id_log'],
                'iva'              => '',
                'id_proveedor'     => $_POST['proveedor'],
                'anio'             => $anio_fecha,
                'justificacion'    => $_POST['justificacion'],
            );

            $confirmar = ModeloSolicitud::actualizarEstadoModel($datos_solicitud);

            if ($confirmar == true) {

                $array_producto = array();
                $array_producto = $_POST['id_producto'];

                $array_precio = array();
                $array_precio = $_POST['valor'];

                $array_nom = array();
                $array_nom = $_POST['nom_producto'];

                $array_cantidad = array();
                $array_cantidad = $_POST['cantidad'];

                $array_iva = array();
                $array_iva = $_POST['iva'];

                $it = new MultipleIterator();
                $it->attachIterator(new ArrayIterator($array_producto));
                $it->attachIterator(new ArrayIterator($array_precio));
                $it->attachIterator(new ArrayIterator($array_nom));
                $it->attachIterator(new ArrayIterator($array_cantidad));
                $it->attachIterator(new ArrayIterator($array_iva));

                foreach ($it as $a) {
                    $id_producto  = $a[0];
                    $precio       = $a[1];
                    $nom_producto = $a[2];
                    $cantidad     = $a[3];

                    $datos_precio = array(
                        'id_producto'  => $id_producto,
                        'nom_producto' => $nom_producto,
                        'cantidad'     => $cantidad,
                        'precio'       => str_replace(',', '', $precio),
                        'iva'          => $a[4],
                    );

                    $actualizar_precio = ModeloSolicitud::preciosProductoControl($datos_precio);
                }

                if ($actualizar_precio == true) {

                    echo ' <script>
                    window . open("' . BASE_URL . 'imprimir/solicitud?solicitud=' . base64_encode($_POST['id_solicitud']) . '")
                    </script> ';

                    echo '
                    <script>
                    ohSnap("Guardado correctamente!", {color:"green", "duration":"1000"});
                    setTimeout(recargarPagina, 1050);

                    function recargarPagina()
                    {
                        window.location.replace("' . $url . '");
                    }
                    </script>
                    ';
                }
            }
        }
    }

    public function verificarSolicitudControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos_verificacion = array(
                'id_solicitud'        => $_POST['id_solicitud'],
                'id_log'              => $_POST['id_log'],
                'cantidad'            => $_POST['cumple_cant'],
                'calidad'             => $_POST['cumple_calidad'],
                'precios'             => $_POST['cumple_precio'],
                'plazos'              => $_POST['cumple_plazo'],
                'observacion_cant'    => $_POST['observacion_cant'],
                'observacion_calidad' => $_POST['observacion_calidad'],
                'observacion_precio'  => $_POST['observacion_precio'],
                'observacion_plazo'   => $_POST['observacion_plazo'],
                'fecha_verificacion'  => $_POST['fecha_verificacion'],
            );

            $verificacion = ModeloSolicitud::verificarSolicitudModel($datos_verificacion);

            if ($verificacion == true) {
                echo ' <script>
                window . open("' . BASE_URL . 'imprimir/solicitud?solicitud=' . base64_encode($_POST['id_solicitud']) . '")
                </script> ';

                echo '
                <script>
                ohSnap("Guardado correctamente!", {color:"green", "duration":"1000"});
                setTimeout(recargarPagina, 1050);

                function recargarPagina()
                {
                    window . location . replace("listado");
                }
                </script>
                ';
            }
        }
    }

    public function removerProductoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {
            $eliminar = ModeloSolicitud::removerProductoModel($_POST['id']);
            return $eliminar;
        }
    }

    public function agregarProductoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {

            $datos = array(
                'id_solicitud' => $_POST['id'],
                'id_log'       => $_POST['id_log'],
                'producto'     => '',
                'cantidad'     => '',
            );

            $guardar = ModeloSolicitud::registrarProdcuctosModel($datos);
            return $guardar;
        }
    }

    public function anularSolicitudControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos = array(
                'id_log'       => $_POST['id_log'],
                'id_solicitud' => $_POST['id_solicitud'],
                'estado'       => 0,
                'motivo'       => $_POST['motivo'],
            );

            $guardar = ModeloSolicitud::anularSolicitudModel($datos);

            if ($guardar == true) {

                echo '
                <script>
                ohSnap("Anulada correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina, 1050);

                function recargarPagina()
                {
                    window.location.replace("listado");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error al anular!", {color:"red", "duration":"1000"});
                </script>
                ';
            }
        }
    }
}
